<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'MainController@index');
Route::get('/old', 'MainController@old');

Route::post('/sendmail', 'SendMailController@send');
Route::post('/sendinquiry', 'SendMailController@inquiry');

Route::get('school-building-infrastructure','FacilityController@building');
Route::get('logout', 'Auth\LoginController@logout');
Route::get('library','FacilityController@library');
/*Route::get('/show','MainController@showmenu');
Route::get('/show','MainController@showmenu');*/


Route::get('science-laborataries','FacilityController@ScienceLab');
Route::get('computer-laboratories','FacilityController@ComputerLab');

Route::get('cultural-activity','FacilityController@CultureActivity');


Route::get('transportation','FacilityController@Transportation');


Route::get('sports','FacilityController@Sports');

Route::get('principle-desk','AccredationController@PrincipleDesk');
Route::get('director-desk','AccredationController@DirectorDesk');
Route::get('history','AccredationController@History');
Route::get('hseb','AccredationController@Hseb');
Route::get('primary-level','AccredationController@PrimaryLevel');
Route::get('secondary-level','AccredationController@SecondaryLevel');
Route::get('admission-procedure','ActivityController@Admission');
Route::get('admission-entrance','ActivityController@AdmissionEntrance');
Route::get('curriculum','ActivityController@Curriculum');

Route::get('attendence','ActivityController@Attendence');
Route::get('scholarship','ActivityController@Scholarship');
Route::get('examination','ActivityController@Examination');
Route::get('staff','ActivityController@Staff');


Route::get('/contact','ContactController@index');

Route::post('/contact-store','ContactController@storing');

Route::get('/about','ContactController@about');


Route::get('/mail-replied/{id}','ContactController@mailChecked');

/*
Route::get('/register-user', 'RegisterController@index');*/
Route::get('/pages', 'PageController@displayPages');
Route::get('/pages/{slug}/show', 'PageController@display');
Route::get('/Catagory/{slug}', 'CategoriesController@display');
Route::get('/display_categories', 'CategoriesController@displaycategories');


Route::get('/posts', 'PostController@show');
Route::get('/post/{slug}/show', 'PostController@display');

Route::get('/blog', 'MainController@blog');
Route::get('/blogstudent', 'MainController@blogstudent');
Route::get('/blogeca', 'MainController@blogeca');
Route::get('/News', 'MainController@News');
Route::resource('/student-form','StudentController');
Route::get('/student-info','StudentController@display');

Route::get('/info-message','StudentController@infomessage');

Route::post('/info-message-store','StudentController@infomessagestore');
/*Route::resource('/teacher-form','TeacherController');
Route::resource('/subject-form','SubjectController');
Route::resource('/marks-insertion','MarksinsertionController');
Route::get('/getstudentFromModel','MarksinsertionController@getstudentFromModel');
Route::get('/getsubjectFromModel','MarksinsertionController@getsubjectFromModel');
Route::get('/check-result','MarksinsertionController@getstudentfromclass');
Route::get('/check-result/{id}','MarksinsertionController@getstudentwithclass');
Route::get('/show/result/{id}','MarksinsertionController@showresult');*/

Route::get('/tinymce_example', function () {
    return view('mceImageUpload::example');
});

Route::get('/tiny', function () {
    return view('test');
});
Auth::routes();

Route::auth();



/*slider part*/

Route::get('/slider', 'SliderController@index')->name('list_slider');
Route::get('/slider/detail/{id}','SliderController@detail')->name('detail_slider');
Route::get('/slider/create','SliderController@create');
Route::post('/slider/create','SliderController@store');
Route:: get('/slider/edit/{id}','SliderController@edit');
Route:: post('/slider/edit/{id}','SliderController@update');
Route:: get('/slider/delete/{id}','SliderController@destroy');



Route::get('/modal', 'ModalController@index');

Route::get('/modal/create','ModalController@create');
Route::post('/modal/create','ModalController@store');

Route:: get('/modal/delete/{id}','ModalController@destroy');



/*Route::get('HomeController@index', '\home')->name('home');*/
/*
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');*/

/*Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    // list all lfm routes here...
});*/









        Route::get('/userinfo', 'ProfileController@displayUser');
        Route::get('dashboard', function () {
            return view('admin-panel/Dashboard');
        });
        Route::get('/laravel-filemanager', '\Unisharp\Laravelfilemanager\controllers\LfmController@show');
        Route::post('/laravel-filemanager/upload', '\Unisharp\Laravelfilemanager\controllers\UploadController@upload');
        Route::resource('/create-user', 'ProfileController');
        Route::get('/student/{id}/edit', 'StudentController@edit');
        Route::get('/student/{id}/delete', 'StudentController@delete');
        Route::get('/menu', 'MainController@menudisplay');
        Route::match(['get', 'post'], '/userinfo/', 'ProfileController@displayUser');
        Route::get('/send-message', 'SendmessageController@sendmessage');
        Route::post('/send-message-store', 'SendmessageController@sendmessagestore');
        Route::get('/post/{id}/edit', 'PostController@edit');
        Route::get('/post/{id}/delete', 'PostController@delete');
        Route::resource('/create-page', 'PageController');
        Route::get('/pages/{id}/edit', 'PageController@edit');
        Route::get('/pages/{id}/delete', 'PageController@delete');
        Route::resource('/create-post', 'PostController');
        Route::get('/change-password/{id}', 'ProfileController@changePassword');
        Route::get('/update-password', 'ProfileController@updatePassword');
        Route::post('/contact-checked', 'ContactController@checked');
        Route::post('/contact-replied', 'ContactController@replied');
        Route::post('/contact-priority', 'ContactController@priority');
        Route::get('/contact-display', 'ContactController@displaycontact');

        Route::get('/userinfo/{id}/edit', 'ProfileController@edit');
        Route::get('/userinfo/{id}/delete', 'ProfileController@delete');
        Route::resource('/create-categories', 'CategoriesController');
        Route::get('/category/{id}/edit', 'CategoriesController@edit');
        Route::get('/category/{id}/delete', 'CategoriesController@delete');



Route::get('/testinomial', 'TestinomialController@index');
Route::get('/add-testinomial','TestinomialController@create');
Route::post('/add-testinomial','TestinomialController@store');
Route::get('/testinomial/{id}/edit','TestinomialController@edit');
Route::post('/testinomial/{id}/edit','TestinomialController@update');
Route::get('/testinomial/{id}/delete','TestinomialController@destroy');

Auth::routes();

/*Route::get('/', 'ChatsController@index');
Route::get('messages', 'ChatsController@fetchMessages');
Route::post('messages', 'ChatsController@sendMessage')/*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/gallery','MainController@galleryPage');



Route::get('/home/gallery', 'GalleryController@index');
Route::get('/home/gallery/create', 'GalleryController@create');
Route::post('/home/gallery/create', 'GalleryController@store');
Route::get('/home/gallery/createVideo', 'GalleryController@createVideo');
Route::post('/home/gallery/createVideo', 'GalleryController@storeVideo');
Route::get('/home/gallery/edit/{id}', 'GalleryController@edit')->name('gallery.edit');
Route::post('/home/gallery/edit/{id}', 'GalleryController@update')->name('gallery.update');
Route::delete('/home/gallery/destroy/{id}', 'GalleryController@destroy')->name('gallery.delete');