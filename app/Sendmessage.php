<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sendmessage extends Model
{
    protected $fillable = [
        'tittle','class_id','message'];
}
