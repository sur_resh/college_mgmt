<?php

namespace App;
use Eloquent;


class Post extends Eloquent
{
    protected $fillable = [
        'author_id','catag_id','role_id', 'title','body','meta_description','meta_keywords','image', 'slug','status',
    ];

    public function category(){
        return $this->belongsTo(Category::class);
    }
}
