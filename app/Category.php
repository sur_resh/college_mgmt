<?php

namespace App;

use Eloquent;

class Category extends Eloquent
{
    protected $fillable = [
        'name', 'body', 'slug',
    ];


    public function post(){
        return $this->hasMany(Post::class);
    }

}
