<?php

namespace App\Http\Controllers;


use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;


class ProfileController extends Controller
{

    public function index()
    {



       //$roles = DB::table('roles')->select('id', 'role', 'created_at')->get();

        $role = DB::table('roles')->pluck( 'role','id');
        return view('auth.register', compact('role'));

    }

    public function store(Request $request)
    {



        $email_exists=DB::table('users')
            ->select('email')
            ->where('email','=',$request->email)->first();



        if(count($email_exists)<=0)

        {
            $featuredImage = Input::file('image');
            $destinationPathFeatured = 'images/profileImages';
            if ($featuredImage != null) {

                $featuredImg = $featuredImage->getClientOriginalName();
                $featuredImage->move($destinationPathFeatured, $featuredImg);
                //save into the database

            }
            else{
                $featuredImg='/earth.jpg';
            }



            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'role_id' => $request->role_id,
                'image_path' => $destinationPathFeatured . '/' . $featuredImg,
                'password' => bcrypt($request->password),
                'api_token' => str_random(60)
            ]);
            return redirect('/create-user')->with("success", "Profile Created Successfully");
        }
        else{
            return redirect('/create-user')->withErrors("User Exists Already, Use different email id and create once again");
        }



        //return redirect('/register')->with("success","Profile Created Successfully");


    }


    public function displayUser()
    {

        $user_infos = DB::table('users')->select('id', 'name', 'email', 'image_path','role_id', 'created_at')->get();

        return view('user_information.user_info',compact('user_infos'));



    }

    public function edit($id)
    {
        $role = [
            '1' => 'Administrator',
            '2' => 'Student',
            '3' => 'Teacher',
            '4' => 'Parents'
        ];
        $userInfos = User::where('id', $id)->first();

        return view('user_information.user_info_edit', compact('userInfos', 'role'));
    }
    public function update(Request $request, $id)
    {


        $featuredImage = Input::file('image');


        $destinationPathFeatured = 'images/profileImages';
        if ($featuredImage != null) {

            $featuredImg = $featuredImage->getClientOriginalName();
            $featuredImage->move($destinationPathFeatured, $featuredImg);
            //save into the database

        }


        $oldFeaturedImageInfo = User::all()->where('id', '=', $id)->first();


        if (isset($featuredImage)) {
            $image_path= $destinationPathFeatured . '/' . $featuredImg;
        } else {
            $image_path = $oldFeaturedImageInfo->avatar;
        }

        User::where('id', $id)
            ->update([
                'name' => $request->name,
                'email' => $request->email,
                'role_id' => $request->role_id,
                'address'=>$request->address,
                'phone_number'=>$request->phone_number,
                'image_path' => $image_path,
                'password' => bcrypt($request->password),
            ]);

        return redirect('userinfo/' . $id . '/edit')->with("success", 'Your Testimonial Has Been Created.');
    }
    public function delete($id)
    {
        User::where('id', $id)->delete();
        return redirect('userinfo/');
    }
    public function changePassword($id){

        $user_infos = DB::table('users')->select('id','role_id', 'name','password', 'email', 'image_path', 'created_at')->where('id','=',$id)->get();

        return view('user_information.change-password',compact('user_infos'));
    }
    public function updatePassword(Request $request, $id){

        User::where('id', $id)
            ->update([
                'password' => bcrypt($request->password),
            ]);


        return redirect('change-password/{id}')->with("success", 'Password Has Been Created.');


    }
}
