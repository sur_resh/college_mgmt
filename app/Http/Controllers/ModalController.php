<?php

namespace App\Http\Controllers;

use App\modal;
use Illuminate\Http\Request;

class ModalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $modal = modal::latest()->get();
        return view('admin-panel.modal.index' , compact('modal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin-panel.modal.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modal = new modal();
        $modal->name = $request['name'];
                if(file_exists($request->photo)){
            
            $filename = 'mod'.time().'.'.$request->photo->getClientOriginalExtension();
            $location = public_path('images/');
            $request->photo->move($location, $filename);
            $modal->photo = $filename;
        }
        else{
          /*  $slider->photo = 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/512/user-male-icon.png';*/
        }
        $modal->save();

        return redirect('/modal');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\modal  $modal
     * @return \Illuminate\Http\Response
     */
    public function show(modal $modal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\modal  $modal
     * @return \Illuminate\Http\Response
     */
    public function edit(modal $modal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\modal  $modal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, modal $modal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\modal  $modal
     * @return \Illuminate\Http\Response
     */
    public function destroy(modal $modal ,$id)
    {
        $modal = modal::find($id)->delete();

        return redirect('/modal');

    }
}
