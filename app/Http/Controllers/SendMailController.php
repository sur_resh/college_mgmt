<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;	
// use App\Mail\SendMail;

class SendMailController extends Controller
{
    function send(Request $request)
    {
     $this->validate($request, [
      'name'     =>  'required',
      'email'  =>  'required|email',
      'subject' =>  'required',
      'phone_number' =>  'required',
      'message' =>  'required'
     ]);

        $data = array(
            'name'      =>  $request->name,
            'subject'   =>   $request->subject,
            'email'   =>   $request->email,
            'phone_number'   =>   $request->phone_number,
            'message'   =>   $request->message
        );
        
        $txt1 = '<html>
        <head>  
        </head>
        <body>
                    <p>Hi, This is '. $data['name'] .'</p>
                    <p>Email:'. $data['email'] .'<br><br>Phone No:'. $data['phone_number'] .'</p>
                    <p>Subject: '. $data['subject'] .' <br><br>
                    Message:<br>
                     '. $data['message'] .'.</p>
                    <p>It would be appriciative, if i receive the response soon.</p>
        </body>
        </html>';       

        $to = "bravo789charlie@gmail.com";
        $subject = "Inquiry Request";

        $headers = "From:sapangleschool.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";


        


         $result=   mail($to,$subject,$txt1,$headers);
                 return back()->with('success','Thanks for contacting us!');
        }


    function inquiry(Request $request)
    {
     $this->validate($request, [
      'first_name'     =>  'required',
      'last_name'  =>  'required|email',
      'phone' =>  'required',
      'message' =>  'required'
     ]);

        $data1 = array(
            'first_name'      =>  $request->first_name,
            'last_name'   =>   $request->last_name,
            'phone'   =>   $request->phone,
            'message'   =>   $request->message
        );
        
        $txt2 = '<html>
        <head>  
        </head>
        <body>
                    <p> '. $data1['first_name'] .' <br><br>'. $data1['last_name'] .'</p>

                    <p><br>Phone No:'. $data1['phone'] .'</p>
                    <br><br>
                    Message:<br>
                     '. $data1['message'] .'.</p>
                    <p>It would be appriciative, if i receive the response soon.</p>
        </body>
        </html>';       

        $to = "bravo789charlie@gmail.com";
        $subject = "Inquiry";

        $headers = "From:sapangleschool.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";


        


         $result=   mail($to,$subject,$txt2,$headers);
                 return back()->with('success','Thanks for contacting us!');
        }




}
