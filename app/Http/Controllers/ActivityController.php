<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    public function Admission()
    {
        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();
        $news=Post::select('title','slug')
            ->where('catag_id', '=', 1)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $upevents=Post::select('title','slug')
            ->where('catag_id', '=', 3)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();

        return view('layouts/activities/Admission',compact('sidebar','catag','news','upevents'));
    }
    public function AdmissionEntrance()
    { $sidebar= Post::select('title','slug')
        -> ORDERBY('created_at','desc')
        ->take(6)
        -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();
        $news=Post::select('title','slug')
            ->where('catag_id', '=', 1)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $upevents=Post::select('title','slug')
            ->where('catag_id', '=', 3)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();

        return view('layouts/activities/AdmissionEntrance',compact('sidebar','catag','news','upevents'));
    }
    public function Curriculum()
    {
        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();
        $news=Post::select('title','slug')
            ->where('catag_id', '=', 1)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $upevents=Post::select('title','slug')
            ->where('catag_id', '=', 3)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        return view('layouts/activities/Curriculum',compact('sidebar','catag','news','upevents'));
    }
    public function Attendence()
    {
        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();
        $news=Post::select('title','slug')
            ->where('catag_id', '=', 1)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $upevents=Post::select('title','slug')
            ->where('catag_id', '=', 3)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        return view('layouts/activities/Attendence',compact('sidebar','catag','news','upevents'));
    }
    public function Scholarship()
    {
        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();
        $news=Post::select('title','slug')
            ->where('catag_id', '=', 1)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $upevents=Post::select('title','slug')
            ->where('catag_id', '=', 3)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        return view('layouts/activities/Scholarship',compact('sidebar','catag','news','upevents'));
    }
    public function Examination()
    {
        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();
        $news=Post::select('title','slug')
            ->where('catag_id', '=', 1)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $upevents=Post::select('title','slug')
            ->where('catag_id', '=', 3)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        return view('layouts/activities/Examination',compact('sidebar','catag','news','upevents'));
    }
    public function Staff()
    {
        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();
        $news=Post::select('title','slug')
            ->where('catag_id', '=', 1)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $upevents=Post::select('title','slug')
            ->where('catag_id', '=', 3)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        return view('layouts/activities/Staff',compact('sidebar','catag','news','upevents'));
    }

}
