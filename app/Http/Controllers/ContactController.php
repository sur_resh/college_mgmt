<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mail;
use App\Mail\ContactMail;

class ContactController extends Controller
{
    public function index()
    {
        return view('layouts.contact');

    }
    public function about()
    {
        return view('layouts.about');

    }

    public function storing(Request $request)
    {
/*dd($request);*/
            Contact::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'subject' => $request->subject,
                'message' => $request->message,

            ]);

        $this->validate($request,[
            'email' => 'required|email',
            'subject' => 'min:3',
        'name'=>'min:5']);

 /*       $data= array(
            'email' => $request->email,
            'subject' => $request->subject,
            'name' => $request->name,
            'phone' => $request->phone_number,
            'bodyMsg' => $request->message
        );*//*
        Mail::send('emails.contact',$data, function($message) use ($data){
$message->from($data['email']);
            $message->to('apex.arish@gmail.com');
            $message->subject($data['subject']);
        });*/
        Mail::to('bravo789charlie@gmail.com')->send(new ContactMail,compact('request'));

            return redirect('/contact')->with("success", " Your Contact Message send Successfully");


        }



    public function checked(Request $request)
    {





        Contact::where('id', $request->msgID)
            ->update([

                'checked' =>1,

            ]);
        return redirect('/contact-display');
    }

    public function unchecked(Request $request)
    {





        Contact::where('id', $request->unchkID)
            ->update([

                'checked' =>0,

            ]);
        return redirect('/contact-display');
    }
    public function replied(Request $request)
    {





        Contact::where('id', $request->repID)
            ->update([

                'replied' =>1,

            ]);
        return redirect('/contact-display');
    }
    public function priority(Request $request)
    {





        Contact::where('id', $request->prioID)
            ->update([

                'priority' =>1,

            ]);
        return redirect('/contact-display');
    }

    public function displaycontact()
    {

        $contact_infos = DB::table('contacts')->select('id', 'name', 'email', 'phone_number','subject','message','checked','replied','priority', 'created_at','updated_at')->get();

        return view('auth.contact_info_redirect',compact('contact_infos'));
    }


    }
