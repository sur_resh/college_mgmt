<?php

namespace App\Http\Controllers;

use App\Classe;
use App\Infomessage;
use App\Student;
use Harimayco\Menu\Facades\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $blood = [
            '1' => 'A+ve',
            '2' => 'A-ve',
            '3' => 'B+ve',
             '4' => 'B-ve',
         '5' => 'AB+ve',
         '6' => 'AB-ve',
         '7' => 'O+ve',
        '8' => 'O-ve'
        ];
        $class_id = DB::table('classes')->pluck('class','id');

        return view('student.student_form_redirect',compact('menuList','blood','class_id'));
    }
    public function infomessage()
    {
        $class_id = DB::table('classes')->pluck('class','id')->toArray();;
        return view('student.student-form-parents_redirect',compact('class_id'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function infomessagestore (Request $request)
    {



       Infomessage::create([
            'tittle' => $request->tittle,
            'class_id' =>$request->class_id ,
            'email' => $request->email,
            'm_name' => $request->m_name,
            'phone_number' => $request->phone_number,
            'message' => $request->message,


        ]);


        $stu_infos = DB::table('classes')->select('class')
            ->where('id','=',$request->class_id)
//            ->leftjoin('classes','id','=',$request->class_id)
            ->first();




        $this->validate($request,[
            'phone_number' => 'required',
            'tittle' => 'min:3',
            'message'=>'min:5']);

        $data= array(
            'tittle' => $request->tittle,
            'class' => $stu_infos,
            'sendername' => $request->m_name,
            'email' => $request->email,
            'phone' => $request->phone_number,
            'bdymsg' => $request->message
        );
        Mail::send('emails.infomessage',$data, function($message) use ($data){
            $message->from($data['email']);
            $message->to('info@nepgeeks.com');
            $message->subject($data['tittle']);
        });


        return redirect('/info-message')->with("success", " Your Contact Message send Successfully");

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     /*   echo $request->name;
        die;*/
        $email_exists = DB::table('students')
            ->select('email')
            ->where('email', '=', $request->email)->first();


        if (count($email_exists) <= 0) {

            Student::create([
                'class_id' => $request->class_id,
                'name' => $request->name,
                'p_name' => $request->p_name,
                'email' => $request->email,
                'pemail' => $request->pemail,
                'p_phone_number' => $request->p_phone_number,
                'phone_number' => $request->phone_number,
                'Blood' => $request->Blood,
                'address' => $request->address,


            ]);
            return redirect('/student-form')->with("success", "Your Form is Submitted Successfully");


        } else {
            return redirect('/student-form')->withErrors("Email Already, Your Form is not Submitted ");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function display()
    {

        //$stu_infos=Student::all();




      $stu_infos = DB::table('students')->select('students.id','class_id','classes.class as classname','name','p_name','phone_number', 'email', 'pemail', 'p_phone_number','Blood','address','students.created_at')
                   ->leftjoin('classes','classes.id','=','students.class_id')
                   ->get();
/*return $stu_infos;*/


        return view('student.display_student_redirect',compact('stu_infos'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blood = [
            'A+ve' => 'A+ve',
            'A-ve' => 'A-ve',
            'B+ve' => 'B+ve',
            'B-ve' => 'B-ve',
            'AB+ve' => 'AB+ve',
            'AB-ve' => 'AB-ve',
            'O+ve' => 'O+ve',
            'O-ve' => 'O-ve'
        ];

        $class_id = DB::table('classes')->pluck('class','id');



        $stu_Infos = Student::where('id', $id)->first();

        return view('student.student_edit_redirect', compact('stu_Infos','blood','class_id'));
    }


    public function update(Request $request, $id)
    {
        $email_exists = DB::table('Students')
            ->select('email')
            ->where('email', '=', $request->email)->first();


        if (count($email_exists) <= 0) {

            Student::where('id', $id)
                ->update([
                    'class_id' => $request->class_id,
                    'name' => $request->name,
                    'p_name' => $request->p_name,
                    'email' => $request->email,
                    'pemail' => $request->pemail,
                    'p_phone_number' => $request->p_phone_number,
                    'phone_number' => $request->phone_number,
                    'Blood' => $request->Blood,
                    'address' => $request->address,
                ]);

            return redirect('student/' . $id . '/edit')->with("success", 'Your Profile Has Been Updated.');
        }
        else {
            return redirect('/student/' . $id . '/edit')->withErrors("Email Already, Your Profile cannot be Updated ");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Student::where('id', $id)->delete();
        return redirect('/student-info');
    }
}
