<?php

namespace App\Http\Controllers;

use App\Sendmessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SendmessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function sendmessage()
    {
        $class_id = DB::table('classes')->pluck('class','id')->toArray();
        return view('student.message_to_parents_redirect',compact('class_id'));

    }

    public function sendmessagestore (Request $request)
    {


        Sendmessage::create([
            'tittle' => $request->tittle,
            'class_id' =>$request->class_id ,
            'message' => $request->message,


        ]);


        $stu_infos = DB::table('classes')->select('class')
            ->where('id','=',$request->class_id)
//            ->leftjoin('classes','id','=',$request->class_id)
            ->first();




        $this->validate($request,[

            'tittle' => 'min:3',
            'message'=>'min:5']);

        $data= array(
            'tittle' => $request->tittle,
            'class' => $stu_infos,
            'bdymsg' => $request->message
        );
       $email = DB::table('students')->where('class_id','=',$request->class_id)->pluck('pemail')->toArray();
        Mail::send('emails.sendmessage',$data, function($message) use ($data,$email){
            $message->from('info@nepgeeks.com');
            $message->to($email);
            $message->subject($data['tittle']);
        });


        return redirect('/send-message')->with("success", " Your Contact Message send Successfully");

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sendmessage  $sendmessage
     * @return \Illuminate\Http\Response
     */
    public function show(Sendmessage $sendmessage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sendmessage  $sendmessage
     * @return \Illuminate\Http\Response
     */
    public function edit(Sendmessage $sendmessage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sendmessage  $sendmessage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sendmessage $sendmessage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sendmessage  $sendmessage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sendmessage $sendmessage)
    {
        //
    }
}
