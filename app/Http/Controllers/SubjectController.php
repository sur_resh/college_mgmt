<?php

namespace App\Http\Controllers;

use App\Subject;
use App\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $class_id = DB::table('classes')->pluck('class','id');

        return view('subject.subject_form_redirect',compact('class_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      /*  echo $request->class_id;
        echo $request->subject1;
        die;*/
  /*    return $request->all();*/
        /*$subjects = $request['subject'];*/


        $subject_exists = DB::table('subjects')
            ->select('subject','class_id')
            ->where(['subject'=> $request->subject ,'class_id'=>$request->class_id])->first();

        /*$subjects = explode(',', $subject);*/

 if (count($subject_exists) <= 0) {
/*print_r($request['subject']);
        die;*/
            $subjects = $request['subject'];
     $full=$request['fullmarks'];
     $pass=$request['pass'];

        $class_id=$request->class_id;
            foreach ($subjects as $key =>$value) {
                if ($value!= null) {


                 Subject::create([
                        'class_id' => $class_id,
                        'subject' => $value,
                     'fmarks' =>$full[$key],
                     'pamarks' =>$pass[$key],
                    ]);
               /* }*/
            }


}
     return redirect('/subject-form')->with("success", "Your Form is Submitted Successfully");
}        else {
            return redirect('/subject-form')->withErrors("Subject for your entered class Already Exists, Your Form is not Submitted !!");
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subject $subject)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject)
    {
        //
    }
}
