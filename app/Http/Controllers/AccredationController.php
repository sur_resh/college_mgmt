<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;

class AccredationController extends Controller
{
public function PrincipleDesk()
{
    $sidebar = Post::select('title', 'slug')
        ->ORDERBY('created_at', 'desc')
        ->take(6)
        ->get();
    $catag = Category::select('name', 'id')
        ->ORDERBY('created_at', 'desc')
        ->take(6)
        ->get();

    return view('layouts/accredation/PrincipleDesk', compact('sidebar', 'catag'));

}
    public function DirectorDesk()
{
    $sidebar= Post::select('title','slug')
        -> ORDERBY('created_at','desc')
        ->take(6)
        -> get();
    $catag=Category::select('name','id')
        ->ORDERBY('created_at','desc')
        ->take(6)
        ->get();
    return view('layouts/accredation/DirectorDesk',compact('sidebar','catag'));
}

    public function History()
    {
        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();

        return view('layouts/accredation/History',compact('sidebar','catag'));
    }
    public function Hseb()
    {
        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();

        return view('layouts/accredation/Hseb',compact('sidebar','catag'));
    }
    public function PrimaryLevel()
    { $sidebar= Post::select('title','slug')
        -> ORDERBY('created_at','desc')
        ->take(6)
        -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();

        return view('layouts/accredation/PrimaryLevel',compact('sidebar','catag'));
    }
    public function SecondaryLevel()
    {
        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();

        return view('layouts/accredation/SecondaryLevel',compact('sidebar','catag'));
    }
}
