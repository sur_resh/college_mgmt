<?php

namespace App\Http\Controllers;

use App\Post;
use App\Slider;
use App\modal;
use App\Gallery;
use App\Category;
use Harimayco\Menu\Facades\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    public function index()
    {
        $menuList = Menu::getByName('top_menu');
        $testinomial=DB::table('testinomials')->get();

        /*return $menuList;*/


        $post=Post::orderBy('id', 'DESC')->get();



        $postt=Post::get();
       $modals = modal::latest()->get();


        $sliders = Slider::all();

        return view('/index',compact('post','sliders','postt','menuList','testinomial','modals'));
    }
    public function old()
    {
        $menuList = Menu::getByName('top_menu');
        $testinomial=DB::table('testinomials')->get();

        /*return $menuList;*/


        $post=Post::get();



        $postt=Post::get();


        $sliders = Slider::all();

        return view('/welcome',compact('post','sliders','postt','menuList','testinomial'));
    }

    public function blog()
    {

        $teacher_post=Post::where('catag_id','5')->get();



        //using pagination method
        return view('layouts.blog.BlogRedirect', ['post' => $teacher_post]);




    }
    public function blogstudent()
    {


        $student_post=Post::where('catag_id','4')->get();
        //using pagination method
        return view('layouts.blog.StudentblogRedirect', ['post' => $student_post]);




    }
    public function blogeca()
    {


        $Eca_post=Post::where('catag_id','6')->get();
        
        return view('layouts.blog.EcaRedirect', ['post' => $Eca_post]);




    }
    public function News()
    {


        $News=Post::where('catag_id','7')->get();
        
        return view('layouts.blog.NewsRedirect', ['post' => $News]);




    }
    public function showmenu()
    {
//  $menuList = Menu::get(1);
        $menuList = Menu::getByName('top_menu');

/*return $menuList;*/
        return view('header',compact('menuList'));

    }

    public function menudisplay()
    {
        $menuList = Menu::getByName('top_menu');
        return view('admin-panel.ourmenu',compact('menuList'));
    }


 public function  galleryPage()
    {
        $gallery = Gallery::latest()->get();
        return view ('gallery', compact('gallery'));
    }
}
