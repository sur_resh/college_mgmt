<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Category;
use App\Post;
use App\User;
use Harimayco\Menu\Facades\Menu;
use Harimayco\Menu\Models\Menus;
use Harimayco\Menu\Models\MenuItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $menuList = Menu::get('1');



        $status = [
            '1' => 'ACTIVE',
            '2' => 'INACTIVE',

        ];
        $categories = Category::pluck('name', 'id');





       return view('admin-panel.CreatePostRedirect',compact('status','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $Aid= Auth::user()->role_id;

        $userinfo=Auth::user();

        $role_id=$userinfo->role_id;
        $author_id=$userinfo->id;




        $slug_exists=DB::table('posts')
            ->select('slug')
            ->where('slug','=',$request->slug)->first();






            $featuredImage = Input::file('image');
            $destinationPathFeatured = 'images/profileImages';
            if ($featuredImage != null) {

                $featuredImg = $featuredImage->getClientOriginalName();
                $featuredImage->move($destinationPathFeatured, $featuredImg);
                //save into the database

            }
            else{
                $featuredImg='/earth.jpg';
            }





            Post::create([
                'title' => $request->title,
                'author_id' =>$author_id,
                'role_id' =>$Aid,
                'catag_id'=>$request->category_id,
                'body' =>$request->body,
                'slug' => $request->slug,
                'meta_description' => $request->meta_description,
                'meta_keywords' => $request->meta_keywords,
                'image' => $destinationPathFeatured . '/' . $featuredImg,
                'status' => $request->status,

            ]);
            return redirect('/create-post')->with("success", "Post Created Successfully");
        }




    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $post_infos = DB::table('posts')->select('posts.id','catag_id','categories.name as name','author_id', 'title', 'posts.body', 'image','posts.slug','status','meta_description','meta_keywords','posts.created_at')
            ->leftjoin('categories','categories.id','=','posts.catag_id')

            ->get();

        $author_name = DB::table('roles')->pluck( 'id','role');
        return view('admin-panel.DisplayPostRedirect',compact('post_infos','author_name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $postInfos = Post::where('id', $id)->first();

        $categories = Category::pluck('name', 'id');
        $status = [
            '1' => 'ACTIVE',
            '2' => 'INACTIVE',

        ];
        return view('admin-panel.PostEditRedirect', compact('postInfos','id','categories','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $featuredImage = Input::file('image');


        $destinationPathFeatured = 'images/profileImages/';
        if ($featuredImage != null) {

            $featuredImg =$destinationPathFeatured.$featuredImage->getClientOriginalName();
            $featuredImage->move($destinationPathFeatured, $featuredImg);
            //save into the database

        }
        else{
            $post_infos = DB::table('posts')->select('id','author_id', 'title', 'body', 'image','slug','status','meta_description','meta_keywords','created_at')->get();



            foreach($post_infos as $info){
                $featuredImg =$info->image ;
            }



        }


        $userinfo=Auth::user();

        $role_id=$userinfo->role_id;
        $author_id=$userinfo->id;
echo  $request->catag_id;


        Post::where('id', $id)
            ->update([
                'title' => $request->title,
                'author_id' =>  $author_id,
                'catag_id' => $request->catag_id,
                'body' => $request->body,
                'slug' => $request->slug,
                'meta_description' => $request->meta_description,
                'meta_keywords' => $request->meta_keywords,
                'image' =>  $featuredImg,
                'status' => $request->status,
            ]);

        return redirect('post/' . $id . '/edit')->with("success", 'Your Testimonial Has Been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function display($slug)
    {
        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();
        $news=Post::select('title','slug')
            ->where('catag_id', '=', 1)
         -> ORDERBY('created_at','desc')
        ->take(6)
        -> get();
$upevents=Post::select('title','slug')
    ->where('catag_id', '=', 3)
    -> ORDERBY('created_at','desc')
    ->take(6)
    -> get();
       // $sluug = DB::table('posts')->pluck( 'id','slug');
        $post_infos = Post::where('slug','=',$slug)
        ->get();


        return view('layouts.PostDisplay.DisplayPostRedirect', compact('post_infos','sidebar','slug','catag','news','upevents'));

    }

    public function delete($id)
    {
        Post::where('id', $id)->delete();
        return redirect('posts/');
    }
}
