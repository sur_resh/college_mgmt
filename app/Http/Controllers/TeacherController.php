<?php

namespace App\Http\Controllers;

use App\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $class_id = DB::table('classes')->pluck('class','id');
        $blood = [
            '1' => 'A+ve',
            '2' => 'A-ve',
            '3' => 'B+ve',
            '4' => 'B-ve',
            '5' => 'AB+ve',
            '6' => 'AB-ve',
            '7' => 'O+ve',
            '8' => 'O-ve'
        ];
      return view('teacher.teacher_form_redirect',compact('class_id','blood'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email_exists = DB::table('teachers')
            ->select('email')
            ->where('email', '=', $request->email)->first();
        $subject =  $request->subject;
        /*$subjects = explode(',', $subject);*/

        if (count($email_exists) <= 0) {

            Teacher::create([
                'class_id' => $request->class_id,
                'name' => $request->name,
                'subject' => $subject,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'Blood' => $request->Blood,
                'address' => $request->address,


            ]);
            return redirect('/teacher-form')->with("success", "Your Form is Submitted Successfully");


        } else {
            return redirect('/teacher-form')->withErrors("Email Already, Your Form is not Submitted ");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teacher $teacher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        //
    }
}
