<?php

namespace App\Http\Controllers;

use App\Classe;
use App\Marksinsertion;
use App\Student;
use App\Studentmark;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class MarksinsertionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $classes = Classe::all();
        $student_id=Student::all();
        /*       $class_id = DB::table('classes')->select('class','id');
               $student_id=DB::table('students')->select('name','id');*/



        return view('student-marks.student_marks_form_redirect',compact('classes','student_id'));
    }
    public function create()
    {


    }
    public function getstudentFromModel(Request $request)
    {
        $student = DB::table("students")
            ->where("class_id", $request->class_id)
            ->select('id','name')
            ->get();
        return json_encode($student);

    }

    public function getsubjectFromModel(Request $request)
    {


        $stu= DB::table("subjects")
            ->where("class_id", $request->class_id)
            ->select('id','subject')
            ->get();
        return json_encode($stu);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

/*return $request->all();*/
        /*$subjects = explode(',', $subject);*/
$student_id=DB::table('marksinsertions')
    ->where(['stu_id'=> $request->name ,'class_id'=>$request->class])->first();

        if(count($student_id)<=0) {
            $subject = $request['sub'];
            $class_id = $request->class;
            $stu_id = $request->name;
            $marks = $request['marks'];
            $fmarks = DB::table("subjects")
                ->where('class_id', $class_id)
                ->pluck('fmarks');
            $tfmarks = 0;
            foreach ($fmarks as $fmark) {
                $tfmarks += $fmark;
            }

            foreach ($subject as $key => $sub) {

                // foreach ($marks as $mark) {


                if ($sub != null) {
//
                    Marksinsertion::create([
                        'class_id' => $class_id,
                        'stu_id' => $stu_id,
                        'subject_id' => $sub,
                        'marks' => $marks[$key],

                    ]);


//              //  return redirect('/marks-insertion')->with("success", "Marks Inserted Successfully");
                    /* }*/
                    // }
//            else{
//                return redirect('/marks-insertion')->withErrors(" Already,Marks not Inserted ");
//            }
                }
            }

            $total = DB::table("marksinsertions")
                ->pluck('marks');
            $tota = 0;
            foreach ($marks as $t) {
                $tota += $t;
            }
            $totall = $tota;
            $percentage = ($totall * 100) / $tfmarks;
            Studentmark::create([
                'class_id' => $class_id,
                'stu_id' => $stu_id,
                'total' => $totall,
                'percentage' => $percentage,

            ]);
            return redirect('/marks-insertion')->with("success", "Marks Inserted Successfully");
        }
        else{
            return redirect('/marks-insertion')->withErrors("Marks for student is already inserted");
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Marksinsertion  $marksinsertion
     * @return \Illuminate\Http\Response
     */
    public function show(Marksinsertion $marksinsertion)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Marksinsertion  $marksinsertion
     * @return \Illuminate\Http\Response
     */

    public function getstudentfromclass(Request $request)
    {
/*    $student=Student::get();
        $subject=Subject::get();
        $studentmarks=Studentmark::get();
        $marks=Marksinsertion::get();*/
     /*   $studentmarks=Studentmark::get();
        $subject=Subject::get();*/
        return view('marksheet.show_student_redirect');
    }
    public function getstudentwithclass($id)
    {
        $student=Student::select('students.class_id','students.id','name','studentmarks.total','studentmarks.percentage')

            ->leftjoin('studentmarks','studentmarks.stu_id','=','students.id')
            ->where('students.class_id',$id)
        ->get();


        return view('marksheet.show_student_id_redirect',compact('student'));
    }
    public function showresult($id)
    {
        $stu_info=Student::select('students.id','name','email','phone_number','address','classes.class')
        ->where('students.id',$id)
            ->leftjoin('classes','classes.id','=','students.class_id')
            -> get();
        $student=Marksinsertion::select('students.name','subjects.subject','subjects.fmarks','subjects.pamarks','marks','classes.class')

    ->leftjoin('students','students.id','=','marksinsertions.stu_id')
->leftjoin('subjects','subjects.id','=','marksinsertions.subject_id')
            ->leftjoin('classes','classes.id','=','marksinsertions.class_id')

->where('students.id',$id)
            ->get();
        $markinfo=Studentmark::select('total','percentage')
            ->where('studentmarks.stu_id',$id)
            ->get();

        return view('marksheet.show_result_student_redirect',compact('student','markinfo','stu_info'));




       /* $subjectInfos=Subject::select('subjects.class_id','subjects.fmarks','subjects.pamarks','subjects.subject')
            ->leftjoin('students.class_id','=','subjects.class_id')
            ->where('subjects.class_id',14)
            ->get();

        return $subjectInfos;*/
    }
    public function edit(Marksinsertion $marksinsertion)
    {


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Marksinsertion  $marksinsertion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marksinsertion $marksinsertion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Marksinsertion  $marksinsertion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marksinsertion $marksinsertion)
    {
        //
    }
}
