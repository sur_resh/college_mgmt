<?php

namespace App\Http\Controllers;

use App\Page;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = [
        '1' => 'ACTIVE',
        '2' => 'INACTIVE',

    ];
        $role = [
            '1' => 'Administrator',
            '2' => 'Student',
            '3' => 'Teacher',
            '4' => 'Parents'
        ];
        return view('admin-panel.page-redirect', compact('status','role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Aid= Auth::user()->role_id;
   /*    echo $Aid;
        die;*/
     /*   $Aid = Auth::user()->id;
echo $Aid;
        die;*/

        $slug_exists=DB::table('pages')
            ->select('slug')
            ->where('slug','=',$request->slug)->first();



        if(count($slug_exists)<=0)

        {
            $featuredImage = Input::file('image');
            $destinationPathFeatured = 'images/profileImages';
            if ($featuredImage != null) {

                $featuredImg = $featuredImage->getClientOriginalName();
                $featuredImage->move($destinationPathFeatured, $featuredImg);
                //save into the database

            }
            else{
                $featuredImg='/earth.jpg';
            }



            Page::create([
                'title' => $request->title,
                'author_id' => $Aid,

                'body' => $request->body,
                'slug' => $request->slug,
                'meta_description' => $request->meta_description,
                'meta_keywords' => $request->meta_keywords,
                'image' => $destinationPathFeatured . '/' . $featuredImg,
                'status' => $request->status,

            ]);
            return redirect('/create-page')->with("success", "Page Created Successfully");
        }
        else{
            return redirect('/create-page')->withErrors("slug Already, Use different slug id and create once again");
        }

    }

    public function displayPages()
    {

        $page_infos = DB::table('pages')->select('id','author_id', 'title', 'body', 'image','slug','status','meta_description','meta_keywords','created_at')->get();
$author_name = DB::table('roles')->pluck( 'id','role');
        return view('admin-panel.display_page_redirect',compact('page_infos','author_name'));



    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*$role = [
            '1' => 'Administrator',
            '2' => 'Student',
            '3' => 'Teacher',
            '4' => 'Parents'
        ];*/
        $status = [
            '1' => 'ACTIVE',
            '2' => 'INACTIVE',

        ];
        $pageInfos = Page::where('id', $id)->first();

        return view('admin-panel.page_edit_redirect', compact('pageInfos', 'role','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $featuredImage = Input::file('image');


        $destinationPathFeatured = 'images/profileImages/';
        if ($featuredImage != null) {

            $featuredImg =$destinationPathFeatured.$featuredImage->getClientOriginalName();
            $featuredImage->move($destinationPathFeatured, $featuredImg);
            //save into the database

        }
        else{
            $page_infos = DB::table('pages')->select('id','author_id', 'title', 'body', 'image','slug','status','meta_description','meta_keywords','created_at')->get();



            foreach($page_infos as $info){
                $featuredImg =$info->image ;
            }



        }




        Page::where('id', $id)
            ->update([
                'title' => $request->title,

                'body' => $request->body,
                'slug' => $request->slug,
                'meta_description' => $request->meta_description,
                'meta_keywords' => $request->meta_keywords,
                'image' =>  $featuredImg,
                'status' => $request->status,
            ]);

        return redirect('pages/' . $id . '/edit')->with("success", 'Your Testimonial Has Been Created.');
    }

    public function display($slug)
    {
$sidebar= Post::selectRaw('title')
        -> ORDERBY('created_at','desc')
        ->take(6)
        -> get();
       /* $slug = DB::table('posts')->pluck( 'id','slug');*/

        $page_infos = Page::where('slug', $slug)->get();
        return view('layouts.PageDisplay.DisplayPageRedirect', compact('page_infos','sidebar','slug'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Page::where('id', $id)->delete();
        return redirect('pages/');
    }
}
