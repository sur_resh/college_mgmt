<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;

class FacilityController extends Controller
{
    public function building()
    {

        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();
        $news=Post::select('title','slug')
            ->where('catag_id', '=', 1)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $upevents=Post::select('title','slug')
            ->where('catag_id', '=', 3)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();

        return view('layouts/facilities/SchoolBuildingInfra',compact('sidebar','catag','news','upevents'));

    }
    public function library()
    {
        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();
        $news=Post::select('title','slug')
            ->where('catag_id', '=', 1)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $upevents=Post::select('title','slug')
            ->where('catag_id', '=', 3)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        return view('layouts/facilities/Library',compact('sidebar','catag','news','upevents'));

    }
    public function ScienceLab()
    {
        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();
        $news=Post::select('title','slug')
            ->where('catag_id', '=', 1)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $upevents=Post::select('title','slug')
            ->where('catag_id', '=', 3)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        return view('layouts/facilities/ScienceLab',compact('sidebar','catag','news','upevents'));

    }
    public function ComputerLab()
    {
        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();
        $news=Post::select('title','slug')
            ->where('catag_id', '=', 1)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $upevents=Post::select('title','slug')
            ->where('catag_id', '=', 3)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        return view('layouts/facilities/ComputerLab',compact('sidebar','catag','news','upevents'));

    }
    public function CultureActivity()
    {
        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();
        $news=Post::select('title','slug')
            ->where('catag_id', '=', 1)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $upevents=Post::select('title','slug')
            ->where('catag_id', '=', 3)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        return view('layouts/facilities/CultureActivity',compact('sidebar','catag','news','upevents'));

    }

    public function Transportation()
    {
        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();
        $news=Post::select('title','slug')
            ->where('catag_id', '=', 1)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $upevents=Post::select('title','slug')
            ->where('catag_id', '=', 3)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        return view('layouts/facilities/Transportation',compact('sidebar','catag','news','upevents'));

    }
    public function Sports()
    {
        $sidebar= Post::select('title','slug')
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $catag=Category::select('name','id')
            ->ORDERBY('created_at','desc')
            ->take(6)
            ->get();
        $news=Post::select('title','slug')
            ->where('catag_id', '=', 1)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        $upevents=Post::select('title','slug')
            ->where('catag_id', '=', 3)
            -> ORDERBY('created_at','desc')
            ->take(6)
            -> get();
        return view('layouts/facilities/Sports',compact('sidebar','catag','news','upevents'));
    }




}
