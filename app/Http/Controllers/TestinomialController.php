<?php

namespace App\Http\Controllers;


use App\testinomial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Services\ImageService;

class TestinomialController extends Controller
{
    /*
     * Image service variable
     */
    protected $imageService;

    /*
     * Constructor for ImageService Class
     */
    public function __construct(ImageService $imageService){
        $this->imageService = $imageService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testinomial = testinomial::all();
        return view('admin-panel.testinomial.index',compact('testinomial'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-panel.testinomial.testinomial_add_redirect');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['image'] = $this->imageService->saveImage($request->file('image'), 'images/profileImages');
        testinomial::create($data);

        return redirect('/add-testinomial')->with("success", "Testinomial Created Successfully");


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\testinomial  $testinomial
     * @return \Illuminate\Http\Response
     */
    public function show(testinomial $testinomial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\testinomial  $testinomial
     * @return \Illuminate\Http\Response
     */
    public function edit(testinomial $testinomial,$id)
    {
        $testinomial = Testinomial::where('id', $id)->first();
       
        return view('admin-panel.testinomial.testinomial_edit_redirect',compact('testinomial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\testinomial  $testinomial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = testinomial::findOrFail($id);
        $input = $request->all();
        if($request->hasFile('image')){
            $input['image'] = $this->imageService->saveImage($request->file('image'), 'images/profileImages');
        } else {
            unset($input['image']);
        }

       $data->update($input);
        
        return redirect()->back()->with("success", "Testinomial Updated Successfully");

}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\testinomial  $testinomial
     * @return \Illuminate\Http\Response
     */
    public function destroy(testinomial $testinomial, $id)
    {
        $testinomial = Testinomial::find($id)->delete();

        return redirect('/testinomial');
    }
}
