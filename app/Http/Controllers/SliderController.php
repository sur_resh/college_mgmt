<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr['slider'] = Slider::all();
        return view('admin-panel.slider.index')->with ($arr);
    }

    public function detail($id){
        $slide= Slider::find($id);
        echo $slide->name;
    }

    public function create(){
        return view('admin-panel.slider.create');

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $slider = new Slider();
        $slider->name = $request['name'];
        $slider->short_intro = $request['short_intro'];
        $slider->intro = $request['intro'];
        if(file_exists($request->photo)){
            
            $filename = 'slide'.time().'.'.$request->photo->getClientOriginalExtension();
            $location = public_path('images/');
            $request->photo->move($location, $filename);
            $slider->photo = $filename;
        }
        else{
          /*  $slider->photo = 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/512/user-male-icon.png';*/
        }
        $slider->save();

        return redirect('/slider');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider, $id)
    {
        $slider = Slider::findorFail($id);
        return view('admin-panel.slider.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = Slider::findorFail($id);
        $slider->name = $request['name'];
        $slider->short_intro = $request['short_intro'];
        $slider->intro = $request['intro'];
        if(file_exists($request->photo)){
            $filename = 'slide'.time().'.'.$request->photo->getClientOriginalExtension();
            $location = public_path('images/');
            $request->photo->move($location, $filename);
            $slider->photo = $filename;
        }
        else{
            $slider->name = $slider->name;
        }
        $slider->save();

        return redirect('/slider');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider ,$id)
    {
        $slider = Slider::find($id)->delete();

        return redirect('/slider');

    }
}
