<?php

namespace App\Http\Controllers;

use App\Gallery;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallery = Gallery::all();
        return view ('layouts.gallery.index',compact('gallery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('layouts.gallery.create');
    }

    public function createVideo()
    {
        return view ('layouts.gallery.createVideo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gallery = new Gallery();
        //  $request->validate([
        //    'name' => 'required',
        //    'image' => 'image:jpg,png,jpeg',
        // ]);
        $gallery->name = $request->name;
        if(file_exists($request->file('image'))){
            $image = "gallery".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/gallery');
            $request->file('image')->move($location, $image);
            $gallery->image = $image;
        }
        else{
            $gallery->image = 'default-thumbnail.png';
        }        
        $gallery->save();
        return redirect('/home/gallery');
    }

    public function storeVideo(Request $request)
    {
        $gallery = new Gallery();
        // $request->validate([
        //     'name' => 'required',
        //     'image' => 'mimetypes:video/mp4,video/avi,video/mpeg,video/mkv,qt  ',
        // ]);
        $gallery->name = $request->name;
        if(file_exists($request->file('image'))){
            $image = "gallery".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/gallery');
            $request->file('image')->move($location, $image);
            $gallery->image = $image;
        }
        else{
            $gallery->image = 'default-thumbnail.png';
        }        
        $gallery->save();
        return redirect('/home/gallery');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gallery  $Gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $Gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gallery  $Gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $Gallery,$id)
    {
        $gallery = Gallery::findOrFail($id);
        return view ('layouts.gallery.edit',compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gallery  $Gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gallery $Gallery,$id)
    {
        $gallery = Gallery::findOrFail($id);
        // $request->validate([
        //     'name' => 'required',
        //     'image' => 'image:jpg,png,jpeg',
        // ]);
        $gallery->name = $request->name;
        if(file_exists($request->file('image'))){
            $image = "gallery".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/gallery');
            $request->file('image')->move($location, $image);
            $gallery->image = $image;
        }
        else{
            $gallery->image = $gallery->image;
        }        
        $gallery->save();
        return redirect('/home/gallery');
    }

    public function updateVideo(Request $request, Gallery $Gallery,$id)
    {
        $gallery = Gallery::findOrFail($id);
        // $request->validate([
        //     'name' => 'required',
        //     'image' => 'mimetypes:video/mp4,video/avi,video/mpeg,video/mkv,qt  ',
        // ]);
        $gallery->name = $request->name;
        if(file_exists($request->file('image'))){
            $image = "gallery".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/gallery');
            $request->file('image')->move($location, $image);
            $gallery->image = $image;
        }
        else{
            $gallery->image = $gallery->image;
        }        
        $gallery->save();
        return redirect('/home/gallery');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gallery  $Gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy( Gallery $Gallery,$id)
    {
        $gallery = Gallery::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
