<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'class_id', 'name', 'p_name','email','pemail','p_phone_number','phone_number','Blood','address'
    ];

    public function classe(){

        return $this->belongsTo(Classe::class);


    }
}
