<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Infomessage extends Model
{
    protected $fillable = [
        'tittle','class_id', 'm_name', 'email','phone_number','message'];
}
