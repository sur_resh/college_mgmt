<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Studentmark extends Model
{
    protected $fillable = [
        'class_id', 'total', 'stu_id','percentage'];
}
