<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marksinsertion extends Model
{
    protected $fillable = [
        'class_id', 'subject_id', 'stu_id','marks'];
}
