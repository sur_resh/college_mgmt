<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = [
        'class_id', 'name', 'subject','email','phone_number','Blood','address'
    ];
}
