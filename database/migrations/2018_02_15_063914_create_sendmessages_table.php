<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendmessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sendmessages', function (Blueprint $table) {
            $table->increments('id');
            $table->text('tittle')->nullable();
            $table->integer('class_id');
            $table->string('message');
            $table->timestamps();
            $table->foreign('class_id')
                ->references('id')
                ->on('classes');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sendmessages');
    }
}
