<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarksinsertionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marksinsertions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stu_id')->unsigned();
            $table->integer('subject_id')->unsigned();
            $table->integer('marks');
$table->integer('class_id')->unsigned();
            $table->timestamps();
            $table->foreign('subject_id')
                ->references('id')
                ->on('subjects');
            $table->foreign('stu_id')
                ->references('id')
                ->on('students');
            $table->foreign('class_id')
                ->references('id')
                ->on('classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marksinsertions');
    }
}
