<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('class_id')->unsigned();
            $table->string('name');
            $table->string('p_name');
            $table->string('email')->unique();
            $table->string('pemail')->unique();
            $table->string('p_phone_number');
            $table->string('phone_number')->nullable();
            $table->enum('Blood', ['A+ve', 'A-ve','B+ve','B-ve','AB+ve','AB-ve','O+ve','O-ve']);
            $table->string('address')->nullable();;

            $table->rememberToken();
            $table->timestamps();
            $table->foreign('class_id')
                ->references('id')
                ->on('classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
