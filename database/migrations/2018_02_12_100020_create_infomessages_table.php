<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateinfomessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infomessages', function (Blueprint $table) {
            $table->increments('id');
            $table->text('tittle')->nullable();
            $table->integer('class_id');
            $table->string('email');
            $table->string('m_name');
            $table->text('phone_number')->nullable();
            $table->text('message')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infomessages');
    }
}
