<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder  {

    public function run()
    {

        DB::Table('roles')->delete();
        /*$roles= array(

                'role' =>   'Administrator'

        );*/
       DB::Table('roles')->insert(
           [
           [
               'role' => 'Administrator'
           ],
           [ 'role' => 'Student'],
           [ 'role' =>  'Teacher'],
           [ 'role' => 'Parents']
               ]
       );
    }
}



?>