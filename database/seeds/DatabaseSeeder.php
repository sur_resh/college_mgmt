<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Database\Migrations;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

Model::unguard();
        // $this->call(UsersTableSeeder::class);
       // $this->call('ProductionSeeder');
        $this->call('RoleTableSeeder');
    }
}
