

<?php

?>
<div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div>
<div class="user-registration">

        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>CREATE NEW USER</h4></div>

                    <div class="panel-body">
                        {!! Form::open(array('url'=>'/create-user/','method'=>'POST','files' => true )) !!}
                        {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                           {{ csrf_field() }}
<div class="row">
    <div class="col-sm-3 margin-left-20">
        {!! Form::label('featured_image','Select Featured Image',array('id'=>'','class'=>'margin-top-20')) !!}
        {!! Form::file('image',array('class'=>'imageUpload','id'=>'uploadFile')) !!}
        <div class="form-group userprofile-bg" id="imagePreview">

        </div>    </div>
    <div class="col-sm-8 ">
        <div class="panel panel-default ">

            <div class="panel-body">

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">Role</label>
                    <div class="col-md-6 col-sm-6">


                        {{ Form:: select('role_id', $role, null, array("class" => "form-control margin-bottom-12", 'placeholder'=>'Select Any Role', 'name' => 'role_id')) }}


                    </div>
                </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>    </div>
        </div>
    </div>
</div>
                        {{ Form:: close() }}
                    </div>
                </div>
            </div>
        </div>

</div>