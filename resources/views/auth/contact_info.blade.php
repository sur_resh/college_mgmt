<div class="row">
<div class="col-sm-12 message-display">

<div class="contact-section display-contacts checked" xmlns="http://www.w3.org/1999/html">

    <div class="table-header">
        <div class="row">


            <div class="col-sm-4 col-md-4 text-left margin-bottom-8">
                <h4>Checked Message</h4>
            </div>




            <div class="clear-both" style="clear:both;"></div>

            <div class="col-md-2 col-sm-2  margin-bottom-0">
                <p class="user-info-head">Name</p>
            </div>
            <div class="col-md-2 col-sm-2 margin-bottom-0">
                <p class="user-info-head">Email</p>
            </div>
            <div class="col-sm-1 margin-bottom-0">
                <p class="user-info-head">Phone</p>
            </div>
            <div class="col-sm-2 margin-bottom-0">
                <p class="user-info-head">Subject</p>
            </div>
            <div class="col-sm-5 margin-bottom-0">
                <p class="user-info-head">Message</p>
            </div>
        </div>
    </div>
    <div class="content-body">
        @foreach($contact_infos as $userinfo)


      @if($userinfo->checked==0)
                @if($userinfo->replied==0)
        <div class="user-row">
            <div class="row">

                    <div class="col-md-2 col-sm-2  margin-bottom-0">
                        <span class="user-red"><b>{{ $userinfo->name}}</b></span>
                    </div>

                    <div class="col-md-2 col-sm-2 margin-bottom-0">
                        <span class="user-red"><b>{{ $userinfo->email}}</b></span>

                    </div>
                    <div class="col-sm-1 margin-bottom-0">
                        {{ $userinfo->phone_number}}
                    </div>
                    <div class="col-sm-2 margin-bottom-0">
                        {{ $userinfo->subject}}
                    </div>
                    <div class="col-sm-5 margin-bottom-0">
                        {{ $userinfo->message}}
                    </div>
<div class="col-sm-12 checked">
<div class="col-sm-9">
</div>
    <div class="col-sm-1">
    <form method='POST' action="/contact-replied">
        {!! csrf_field() !!}

        {{--<a href="mail-replied/{{ $userinfo->id}}" onclick="return confirm('Are you sure you want to change it as checked?')">Replied?</a>--}}
        <button name="repID" type="submit" value={{ $userinfo->id}} onclick="return confirm('Are you sure you want to change it as Replied?')">Replied</button>
        {!! csrf_field() !!}
    </form>
        </div>
       {{-- <div class="col-sm-2">
    <form method='POST' action="/contact-priority">
        {!! csrf_field() !!}

        --}}{{--<a href="mail-replied/{{ $userinfo->id}}" onclick="return confirm('Are you sure you want to change it as checked?')">Replied?</a>--}}{{--
        <button name="prioID" type="submit" value={{ $userinfo->id}} onclick="return confirm('Are you sure you want to change it as High Priority?')">High Priority</button>

    </form>

            </div>--}}
</div>

                </div>

            </div>
         @endif()
@endif()
        @endforeach()
    </div>
</div>



<div class="replied-message" >

    <div class="table-header">
        <div class="row">


            <div class="col-sm-4 col-md-4 text-left margin-bottom-8">
                <h4>Repiled Message</h4>
            </div>




            <div class="clear-both" style="clear:both;"></div>

            <div class="col-md-2 col-sm-2  margin-bottom-0">
                <p class="user-info-head">Name</p>
            </div>
            <div class="col-md-2 col-sm-2 margin-bottom-0">
                <p class="user-info-head">Email</p>
            </div>
            <div class="col-sm-1 margin-bottom-0">
                <p class="user-info-head">Phone</p>
            </div>
            <div class="col-sm-2 margin-bottom-0">
                <p class="user-info-head">Subject</p>
            </div>
            <div class="col-sm-5 margin-bottom-0">
                <p class="user-info-head">Message</p>
            </div>
        </div>
    </div>
    <div class="content-body">
        @foreach($contact_infos as $userinfo)


            @if($userinfo->replied==1)

                <div class="user-row">
                    <div class="row">

                        <div class="col-md-2 col-sm-2  margin-bottom-0">
                            <span class="user-red"><b>{{ $userinfo->name}}</b></span>
                        </div>

                        <div class="col-md-2 col-sm-2 margin-bottom-0">
                            <span class="user-red"><b>{{ $userinfo->email}}</b></span>

                        </div>
                        <div class="col-sm-1 margin-bottom-0">
                            {{ $userinfo->phone_number}}
                        </div>
                        <div class="col-sm-2 margin-bottom-0">
                            {{ $userinfo->subject}}
                        </div>
                        <div class="col-sm-5 margin-bottom-0">
                            {{ $userinfo->message}}
                        </div>

                        <div class="col-sm-12 checked">
                            <div class="col-sm-9">
                            </div>
                            <div class="col-sm-2">
                                <form method='POST' action="/contact-priority">
                                    {!! csrf_field() !!}

                                    {{--<a href="mail-replied/{{ $userinfo->id}}" onclick="return confirm('Are you sure you want to change it as checked?')">Replied?</a>--}}
                                  {{--  <button name="prioID" type="submit" value={{ $userinfo->id}} onclick="return confirm('Are you sure you want to change it as High Priority?')">High Priority</button>--}}

                                </form>

                            </div>

                        </div>
</div>
                    </div>


            @endif()

        @endforeach()
    </div>
    </div>


</div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
