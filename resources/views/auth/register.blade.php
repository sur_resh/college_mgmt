

@extends('layouts.app')

@section('content')
<div class="row">
        <div class="col-sm-3">
            @include('admin-panel.left-nav')
    </div>
    <div class="col-sm-9">
        @include('auth.register-form')
    </div>
</div>
    <script type="text/javascript" src="js/imagePreview.js"></script>
@endsection


