
<?php

?>
{{--<style>
    .feed {padding: 5px 0}
</style>
<form method="post" action="new_ele2.php" onsubmit="return validate(this)">
    <table>
        <tr>
            <td valign=top> Feed URL (s):</td>
            <td valign=top>
                <div id="newlink">
                    <div class="feed">
                        <input type="text" name="feedurl[]" value="http://feeds.feedburner.com/satya-weblog/scripting" size="50">
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <p>
        <br>
        <input type="submit" name="submit1">
        <input type="reset" name="reset1">
    </p>
    <p id="addnew">
        <a href="javascript:add_feed()">Add New </a>
    </p>
</form>--}}
{{--<script type="text/javascript">
    function validate(frm)
    {
        var ele = frm.elements['feedurl[]'];
        if (! ele.length)
        {
            alert(ele.value);
        }
        for(var i=0; i<ele.length; i++)
        {
            alert(ele[i].value);
        }
        return true;
    }
    function add_feed()
    {
        var div1 = document.createElement('div');
        // Get template data
        div1.innerHTML = document.getElementById('newlinktpl').innerHTML;
        // append to our form, so that template data
        //become part of form
        document.getElementById('newlink').appendChild(div1);
    }
</script>--}}
<script type="text/javascript" src="/js/textboxcreationdynamic.js"></script>
<div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div>


<!-- Template. This whole data will be added directly to working form above -->
<div id="newlinktpl" style="display:none">
    <div class="feed">
        <input type="text" name="feedurl[]" value=""  size="50">
    </div>
</div>
<div class="student-form">

    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h4> SUBJECT FORM</h4></div>

                <div class="panel-body">
                    {!! Form::open(array('url'=>'/subject-form','method'=>'POST','files' => true )) !!}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="col-sm-12 ">
                            <div class="panel panel-default ">

                                <div class="panel-body">

                                    <div class="form-group">
                                        <label for="class_id" class="col-md-4 control-label">Class </label>
                                        <div class="col-md-6 col-sm-6">

                                            {{ Form:: select('class_id',$class_id,null,array("class" => "form-control margin-bottom-12", 'placeholder'=>'Select Any Class', 'id' => 'class_id')) }}


                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-top">
                                        <div id='TextBoxesGroup'>
                                            <div id="TextBoxDiv1">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                    <div class="col-md-4 col-sm-6">
                                                    <label>Subject #1 : </label>
                                                        </div>
                                                    <div class="col-md-4 col-sm-4">

                                                    <input type='textbox' class="form-control"  name="subject[]" id='subject1'>
                                                        </div>
                                                    </div>
                                                    </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-2"></div>
                                                        <div class="col-md-2 col-sm-2">
                                                            <label>Full Marks : </label>
                                                        </div>
                                                    <div class="col-md-4 col-sm-4">

                                                        <input type='textbox' class="form-control"  name="fullmarks[]" id='fm1'>
                                                    </div>
                                                        </div>
                                                    </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-2"></div>
                                                        <div class="col-md-2 col-sm-2">
                                                            <label>Pass Marks : </label>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4">

                                                            <input type='textbox' class="form-control"  name="pass[]" id='pass1'>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            </div>
                                        <input type='button' value='Add Button' id='addButton'>
                                        <input type='button' value='Remove Button' id='removeButton'>


                                    <div class="clear" style="clear:both"></div>
                                {{--    <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                                        <label for="subject" class="col-md-4 control-label">Subject</label>

                                        <div class="col-md-6">
                                            <input id="subject" type="text" class="form-control" name="subject" value="{{ old('subject') }}" required autofocus>

                                            @if ($errors->has('subject'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>--}}

                                   {{-- <p id="addnew">
                                        <a href="javascript:add_feed()">Add New </a>
                                    </p>--}}


                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Submit
                                            </button>
                                        </div>
                                    </div>    </div>
                            </div>
                        </div>
                    </div>
                    {{ Form:: close() }}


                </div>
            </div>
        </div>
    </div>

</div>