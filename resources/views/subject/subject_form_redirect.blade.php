@extends('layouts.mainpage.app')

@section('content')
    <div class="row">
        <div class="col-sm-3">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-9">

            @include('subject.subject_form')
        </div>
    </div>

@endsection