
<?php
$posts = DB::table('posts')->orderby('id','DESC')->get();
?>


<footer class="ftco-footer ftco-bg-dark ftco-section">
<div class="container">
<div class="row mb-5">
<div class="col-md-6 col-lg-3">
<div class="ftco-footer-widget mb-5">
<h2 class="ftco-heading-2">Connect to us</h2>
<div class="block-23 mb-3">
<ul>
<li><span class="icon icon-map-marker"></span><span class="text">Kalikasthan, Kathmandu</span></li>
<li><a href="tel:+977014424885"><span class="icon icon-phone"></span><span class="text">+977-01-4424885</span></a></li>
<li><a href="mailto:spangleschool@gmail.com"><span class="icon icon-envelope"></span><span class="text">spangleschool@gmail.com</span></a></li>
</ul>

</div>
</div>
</div>
<div class="col-md-6 col-lg-3">
<div class="ftco-footer-widget mb-5">
<h2 class="ftco-heading-2">Recent Blog</h2>

<?php
                if(count($posts)>0)
                {
                foreach($posts as $key=>$pos){
                ?>
                @if($key <= 1)
<div class="block-21 mb-4 d-flex">
<a class="blog-img mr-4" style="background-image: url('/<?php echo $pos->image; ?>');"></a>
<div class="text">
<h3 class="heading"><a href="{{url("post/$pos->slug/show")}}"><?php echo str_limit($pos->title, $limit = 37, $end = '..');  ?> </a></h3>
<div class="meta">
<!-- <div><a href="#"><span class="icon-calendar"></span> Dec 25, 2018</a></div>
<div><a href="#"><span class="icon-person"></span> Admin</a></div>
<div><a href="#"><span class="icon-chat"></span> 19</a></div> -->
</div>
</div>
</div>
@endif
<?php

}
}
?>

</div>
</div>
<div class="col-md-6 col-lg-3">
<div class="ftco-footer-widget mb-5 ml-md-4">
<h2 class="ftco-heading-2">Links</h2>
<ul class="list-unstyled">
<li><a href="/"><span class="lnr lnr-arrow-right"></span>Home</a></li>
<li><a href="/about"><span class="lnr lnr-arrow-right"></span>About</a></li>
<li><a href="#"><span class="lnr lnr-arrow-right"></span>Services</a></li>
<li><a href="#"><span class="lnr lnr-arrow-right"></span>Deparments</a></li>
<li><a href="/contact"><span class="lnr lnr-arrow-right"></span>Contact</a></li>
</ul>
<div class="ftco-footer-widget mb-5">
<h2 class="ftco-heading-2 mb-0">Social Links</h2>
<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-3">
<li class="ftco-animate"><a href="https://www.facebook.com/spangleboarding/"><span class="icon-facebook"></span></a></li>
<li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
</ul>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3">
<div class="ftco-footer-widget mb-4">
		<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fspangleboarding%2F&tabs=timeline&width=340&height=400&small_header=true&adapt_container_width=false&hide_cover=false&show_facepile=true&appId" width="340" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

</div>
</div>
</div>
<div class="row">
	<div class="col-sm-12">
	</div>
	</div>
<div class="row">
<div class="col-md-12 text-center">
<p>
Copyright &copy;<script type="text/javascript">document.write(new Date().getFullYear());</script> All rights reserved | Made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://www.nepgeeks.com" target="_blank">Nepgeeks Technology</a>
</p>
</div>
</div>
</div>
</footer>
