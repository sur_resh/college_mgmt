<div class="py-2 bg-primary">
	<div class="container">
		<div class="row no-gutters d-flex align-items-start align-items-center px-3 px-md-0">
			<div class="col-lg-12 d-block">
				<div class="row d-flex">
					<div class="col-md-5 pr-4 d-flex topper align-items-center">
						<div class="icon bg-fifth mr-2 d-flex justify-content-center align-items-center"><span class="icon-map"></span></div>
						<span class="text">	Kalikasthan, Kathmandu</span>
					</div>
					<div class="col-md pr-4 d-flex topper align-items-center">
						<div class="icon bg-secondary mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
						<span class="text">	spangleschool@gmail.com</span>
					</div>
					<div class="col-md pr-4 d-flex topper align-items-center">
						<div class="icon bg-tertiary mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
						<span class="text">	+977-01-4424885</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco_navbar ftco-navbar-light" id="ftco-navbar">
	<div class="container d-flex align-items-center">
		<div class="navbar-header menuitems">
			<a href="/" class="logoschool"><img src="{{asset('/images/spangle-logo.jpg')}}"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
				Menu
			</button>
			{{-- <a class="navbar-brand" href="#">Spangle School</a>--}}
		</div>
		<div class="collapse navbar-collapse" id="ftco-nav">
			@foreach($menuList as $pageinfo=>$value)

			<ul class="navbar-nav ml-auto">

				@if(count($value['child'])>0)
				<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ $value['label']}} <!-- <span class="caret"></span> --></a>
					<ul class="dropdown-menu">
						@foreach($value['child'] as $childkey=>$childValue)
						<a href="{{$childValue['link']}}"><li>{{$childValue['label']}}<div class="border_menu"></div></li></a>
						@endforeach
					</ul>
				</li>
				@else
				<li class="nav-item active"><a class="nav-link pl-0" href="{{ $value['link']}}">{{ $value['label']}}</a>
				</li>
				@endif
			</ul>
		@endforeach
		</div>
	</div>
</nav>