@if(Session::has('success'))
    <div class="alert alert-success">
        <strong>Congratulation! </strong>{{Session::get('success')}}
    </div>
@endif