<div class="footer">
    <div class="container">
        <div class="row">

                <div class="col-sm-4">
                    <h3>
                        Spangle Bording School</h3>
                    <ul>
                        <li>Email us:<a href=""> spangleschool@gmail.com</a></li>
                        <li>Contact us:<a href=""> 01-4424885</a></li>
                        <li>Visit us: Kalikasthan, kathmandu</li>

                    </ul>

                </div>
                <div class="col-sm-4">
                    <h3>
                        Customer Services
                    </h3>
                    <ul>
                        <li><a href="">Contact Us</a></li>
                        <li><a href="">Delivery Information</a></li>
                        <li><a href="">Return Policy</a></li>

                    </ul>
                </div>
                <div class="col-sm-4">
                    <h3 >
                        Community
                    </h3>
                    <ul>
                        <li><a href="">My Account</a></li>
                        <li><a href="">Affiliates</a></li>
                        <li><a href="">Blog</a></li>


                    </ul>
                </div>
        </div>
        <div class="row">
            <div class="footer_second" style="margin:0px auto">
                <div class="col-sm-12">©Copyright Reserved The Spangle Boarding High School 2019</div>
                <div class="col-sm-12">Powered By: <a href="https://nepgeeks.com/">Nepgeeks Technology</a> </div>
            </div>
        </div>
    </div>
</div>