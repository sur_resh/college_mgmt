<div id="tophead">
    <div class="container-fluid">
        <div class="col-sm-4 top-news">
						<span class="top-news-title">
												Call Us:</span>
            <div id="news-ticker">
                <div class="news-ticker-inner-wrap">



                    <div class="list" style="margin: 0px; display: block;">
                        <a href="tel:'01-4424885'">01-4424885</a>
                    </div></div> <!-- .news-ticker-inner-wrap -->
            </div><!-- #news-ticker -->
        </div> <!-- #top-news -->



        <div class="col-sm-8 top-nav">


            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        {{-- <a class="navbar-brand" href="#">Spangle School</a>--}}
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        @foreach($menuList as $pageinfo=>$value)

                            <ul class="nav navbar-nav">

                                @if(count($value['child'])>0)
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ $value['label']}} <!-- <span class="caret"></span> --></a>
                                        <ul class="dropdown-menu">
                                            @foreach($value['child'] as $childkey=>$childValue)
                                                <li><a href="{{$childValue['link']}}">{{$childValue['label']}}</a></li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @else
                                    <li class="active"><a href="{{ $value['link']}}">{{ $value['label']}}</a>

                                    </li>
                                @endif

                            </ul>
                        @endforeach

                    </div>
            </nav>
         			</div> <!-- #top-nav -->
    </div> <!-- .container -->

</div>