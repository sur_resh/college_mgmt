@extends('layouts.mainpage.app')

@section('content')
    <!-- <div class="row">
        <div class="col-sm-12">
            @include('layouts.error_message')
            @include('layouts.sucess_message')
        </div>
    </div> -->
<div class="jumbotron jumbotron-sm contact-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h1 class="">
                   <small>Feel free to contact us</small>
                </h1>
            </div>
        </div>
    </div>
</div>
<section class="contact-us">
<div class="container">
    <div class="row">
        <div style="background: white;" class="col-md-8 contactus">
            <div class="well well-sm back-ground-none">
                <form action="/sendmail" method="POST">
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">
                                    Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" required="required" />
                            </div>
                            <div class="form-group">
                                <label for="email">
                                    Email Address</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required="required" />
                            </div>
                            <div class="form-group">
                                <label for="phone">
                                    Contact Number</label>

                                    <input type="tel" class="form-control" id="phone" name="phone_number" placeholder="Enter contact number" required="required" />
                            </div>
                          <div class="form-group">
                                <label for="subject">
                                    Subject</label>
                                <textarea id="subject" name="subject" class="form-control" required="required"></textarea>
                                    

                                </select>
                            </div> 
                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="name">
                                    Message</label>
                            <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
                                      placeholder="Message"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                                Send Message</button>
                        </div>
                        <div class="col-md-12">

                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4 contact-address">
            <form>
                <legend><span class="glyphicon glyphicon-globe"></span> Our office</legend>
                <address>
                    <strong>Spangle Boarding School</strong><br>
                   Kalikasthan,Kathmandu
                </address>
                <address>
                    <strong>Email Us</strong><br>
                    <a href="mailto:spangleschool@gmail.com">spangleschool@gmail.com</a>
                </address>
                <address>
                    <strong>Call Us</strong><br>
                    <a href="tel:01-4424885">01-4424885</a>
                </address>
            </form>
        </div>
    </div>
</div>
</section>
@endsection