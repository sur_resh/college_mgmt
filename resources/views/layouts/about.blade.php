@extends('layouts.mainpage.app')

@section('content')
    <section class="about">


    </section>
    <div class="container">
        <div class="row">

            <div class="col-sm-12 right-information">

                <section class="school-building-details">


                        <header class="entry-header">
                            <h1 class="entry-title">About Us</h1>	</header><!-- .entry-header -->

                        <div class="entry-content-wrapper">
                            <div class="entry-content">
                                <p><a href="http://myschool.nepgeeks.com/wp-content/uploads/2017/05/school.jpg"><img src="http://myschool.nepgeeks.com/wp-content/uploads/2017/05/school.jpg" alt="" width="2384" height="472" class="aligncenter size-full wp-image-295" srcset="http://myschool.nepgeeks.com/wp-content/uploads/2017/05/school.jpg 2384w, http://myschool.nepgeeks.com/wp-content/uploads/2017/05/school-300x59.jpg 300w, http://myschool.nepgeeks.com/wp-content/uploads/2017/05/school-768x152.jpg 768w, http://myschool.nepgeeks.com/wp-content/uploads/2017/05/school-1024x203.jpg 1024w, http://myschool.nepgeeks.com/wp-content/uploads/2017/05/school-400x79.jpg 400w" sizes="(max-width: 2384px) 100vw, 2384px"></a><br>
                                    The foundation stone of the building was laid by H.E. The Vice President of India and the building was inaugurated on May 14, 1993 by H.E. The Honourable President of India, Dr. Shankar Dayal Sharma.</p>
                                <p>The very best of facilities have been provided to the teachers and students of St George’s regardless of cost .Latest teaching aids such as electronic smart board, tablet computers and mobile aids are regularly used for the teaching-learning process at the school.</p>
                                <p>Additionally, nearly an entire floor of the school has been used for the science laboratories and the library. There are separate rooms for teaching classical music and western music.The senior classrooms, dance studio and the art room of the school have been recently renovated .Students also have access to an RO (Reverse Osmosis) drinking water system, which has several water points on each floor.</p>
                                <p>The school also has a state-of-the-art staff training center, where teacher training workshops are organized throughout the year. This is to constantly widen subject familiarity as well be updated with the latest teaching methodologies and evaluation tools.</p>
                                <p>St George’s School continuously strives to upgrade the technology infrastructure –the addition of over 65 new computers for the senior labs has been a recent happening in this regard, as well as supporting Wi-Fi connectivity in the whole school. A school administrative software enables the accurate and efficient functioning of the school system. The software takes care of the Time Table generation, Examination, Payroll and Student &amp; Staff Information System.</p>
                                <p>The infrastructure of St George’s School is thus a valuable asset directed towards building a student community par excellence</p>
                            </div><!-- .entry-content -->
                        </div><!-- .entry-content-wrapper -->



                </section>


            </div>

        </div>
    </div>






@endsection