<section class="display-page-details">

<div class="container-fluid">
    <div class="row">

    @foreach($page_infos as $post_info)

        <img src="<?php echo '/'.$post_info->image;?>">
    <div class="page-wrap">
        <h4 class="display-post-details"><?php echo $post_info->title;?></h4>

        <p><?php echo $post_info->body;?></p>
       
    </div>
    @endforeach

    </div>
    </div>
</section>