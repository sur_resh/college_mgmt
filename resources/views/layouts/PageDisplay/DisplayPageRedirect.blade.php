@extends('layouts.mainpage.app')

@section('content')
<div class="row">
            <div class="inner-page col-sm-9">
                @include('layouts.PageDisplay.DisplayPage')
            </div>
<div class="sidebar col-sm-3">
    @include('admin-panel.SideBar')
</div>
</div>
    <script type="text/javascript" src="js/imagePreview.js"></script>
@endsection
