<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('appp.name', 'Spangle') }}</title>

    <!-- Styles -->
   <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="{{asset('css/open-iconic-bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('css/animate.css')}}">
<link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
<link rel="stylesheet" href="{{asset('css/aos.css')}}">
<link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">
<link rel="stylesheet" href="{{asset('css/flaticon.css')}}">
<link rel="stylesheet" href="{{asset('css/icomoon.css')}}">
<link rel="stylesheet" href="{{asset('css/index-style.css')}}">
 <link rel="stylesheet" href="{{asset('css/gallery-grid.css')}}">
    <script type="javascript">

        $('#txt_email').keyup(function () {

            alert('xzcjv');

            var str = $(this).val();
            str= str.toLowerCase();
            str = str.replace(/\s+/g, "-");

            $("#txt_username").val(str);

        });


    </script>
    <script>
        var menus = {
            "oneThemeLocationNoMenus" : "",
            "moveUp" : "Move up",
            "moveDown" : "Mover down",
            "moveToTop" : "Move top",
            "moveUnder" : "Move under of %s",
            "moveOutFrom" : "Out from under  %s",
            "under" : "Under %s",
            "outFrom" : "Out from %s",
            "menuFocus" : "%1$s. Element menu %2$d of %3$d.",
            "subMenuFocus" : "%1$s. Menu of subelement %2$d of %3$s."
        };
        var arraydata = [];
        var addcustommenur= '{{ route("haddcustommenu") }}';
        var updateitemr= '{{ route("hupdateitem")}}';
        var generatemenucontrolr= '{{ route("hgeneratemenucontrol") }}';
        var deleteitemmenur= '{{ route("hdeleteitemmenu") }}';
        var deletemenugr= '{{ route("hdeletemenug") }}';
        var createnewmenur= '{{ route("hcreatenewmenu") }}';
        var csrftoken="{{ csrf_token() }}";
        var menuwr = "{{ url()->current() }}";

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrftoken
            }
        });
    </script>
    
</head>
<body>
<div id="app">
 <?php

    use Harimayco\Menu\Facades\Menu;
    $menuList = Menu::getByName('top_menu');


    ?>
    @include ("layouts.mainpage.header")

    @yield('content')

    @yield('homeContent')
    @yield('posts')

    @yield('nav')
    @yield('contents')
</div>



<!-- Scripts -->
<script src="{{ asset('app.js') }}"></script>
<script type="text/javascript" src="{{asset('vendor/harimayco-menu/scripts.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/harimayco-menu/scripts2.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/harimayco-menu/menu.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="http://skincare.nepgeeks.com/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
                <script>
                                            CKEDITOR.replace( 'ckeditor_text' );
                                            </script>
                                            <style>
    .pb-video-container {
        padding-top: 20px;
        background: #bdc3c7;
        font-family: Lato;
    }

    .pb-video {
        border: 1px solid #e6e6e6;
        padding: 5px;
    }

        .pb-video:hover {
            background: #0f0d35;
        }

    .pb-video-frame {
        transition: width 2s, height 2s;
    }


    .pb-row {
        margin-bottom: 10px;
    }
    .process video {
    max-width: 50%;
}
button.btn.btn-primary.make-btn {
    font-size: 25px;
}
button.btn.btn-danger {
    font-size: 20px;
}
</style>
</body>
</html>
