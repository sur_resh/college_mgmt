<section class="latest-event">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12">
                    <div class="title-course">Teacher Posts</div>
                </div>
                @foreach($post as $key=>$pos)
                <div class="col-sm-4 blog-next-homepage">
                    <div class="rodfw">
                        <img src=" <?php echo $pos->image; ?>">
                        <h4><a href="{{url("post/$pos->slug/show")}}"><?php echo str_limit($pos->title, $limit = 37, $end = '..');   ?></a></h4>
                        <p><div class="blog_para"><?php  echo str_limit($pos->body, $limit = 100, $end = '...');   ?></div> </p>
                    </div>
                </div>
                    @endforeach
            </div>
        </div>
    </div>
</section>