

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <?php $sliderCount = 0; ?>
    @foreach($sliders as $slide)
        <li data-target="#carouselExampleIndicators" data-slide-to="{{ $sliderCount++ }}" class="{{ $loop->first ? 'active' : '' }}"></li>
    @endforeach
  </ol>
  <div class="carousel-inner">
    @foreach($sliders as $slide)
    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
      <img class="d-block w-100" src="/images/{{ $slide->photo}}" alt="{{ $slide->name }}">
    </div>
    @endforeach
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<div class="blue-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <p class="section-title">Join Our School</p>
                <p class="section-body">
                The objective of school is to develop students into competent citizens for any sector of organized
                activity. The student should have an opportunity to obtain a broad knowledge of the concepts and
                reality-based skills which we promise to provide.
                </p>
            </div>
            <div class="col-sm-4 text">
                <a href="/contact" class="learn-more">Contact Us</a><a href="#" class="learn-more">Application Form</a>
            </div>
        </div>

    </div><br>
</div>
<section class="latest-event">
    <div class="container">
        <div class="row">

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <div class="title-course">Teacher Blog</div>
                </div>

                <?php

                if(count($post)>0)

                {



                $i=0;
                foreach($post as $key=>$pos){

                if($key<count($post))
                {


                // use $i as index
                // use $athletes[$i]

                if($pos->catag_id=='5')
                {
                if($i<4)
                {


                ?>

                <div class="col-sm-6 blog-next-homepage">
                    <div class="rodfw">

                        <img src=" <?php echo $pos->image; ?>">


                        <h4><a href="{{url("post/$pos->slug/show")}}"><?php echo str_limit($pos->title, $limit = 37, $end = '..');  ?></a></h4>
                        <p><?php  echo str_limit($pos->body, $limit = 150, $end = '...');  ?></p>

                    </div>



                </div>



                <?php

                } $i=$i+1;
                }
                }
                }
                }
                ?>
            </div>


            <div class="col-sm-4">
                <div class="col-sm-12">
                    <div class="title-course">Student Blog</div>
                </div>
                <?php
                if(count($post)>0)

                {
                $i=0;

                foreach($post as $key=>$pos){

                if($key<count($post))
                {

                // use $i as index
                // use $athletes[$i]

                if($pos->catag_id=='4')
                {

                if($i<6)
                {


                ?>

                <div class="col-sm-12 event-upcomming">
                    <div class="upcomming-events" >
                        <div class="row" style="padding: 5px;">
                            <div class="col-sm-4">
                        <img style="height: 80px; " src=" <?php echo $pos->image; ?> "></div>
                            <div class="col-sm-8">
                        <h4><a href="{{url("post/$pos->slug/show")}}"><?php echo str_limit($pos->title, $limit = 36, $end = '..');   ?></a></h4>
                        <p><?php  echo str_limit($pos->body, $limit = 70, $end = '...');  ?></p>
                            </div>
                        </div>
                    </div>



                </div>



                <?php

                }$i=$i+1;
                }
                }
                }
                }
                ?>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <div class="title-course">ECA</div>
                </div>

                <?php

                if(count($post)>0)

                {



                $i=0;
                foreach($post as $key=>$pos){

                if($key<count($post))
                {


                // use $i as index
                // use $athletes[$i]

                if($pos->catag_id=='6')
                {
                if($i<4)
                {


                ?>

                <div class="col-sm-6 blog-next-homepage">
                    <div class="rodfw">

                        <img src=" <?php echo $pos->image; ?>">


                        <h4><a href="{{url("post/$pos->slug/show")}}"><?php echo str_limit($pos->title, $limit = 37, $end = '..');  ?></a></h4>
                        <p><?php  echo str_limit($pos->body, $limit = 150, $end = '...');  ?></p>

                    </div>



                </div>



                <?php

                } $i=$i+1;
                }
                }
                }
                }
                ?>
            </div>

        </div>
    </div>
</section>

<div style="clear:both;"></div>
<section class="home-parallex" style="">
    <div class="home-parallax">
        <div class="parallax-window" data-parallax="scroll" data-image-src="{{asset('images/gallary/9.JPG')}}">
            <div class="parallax-text">We believe in building a positive environment which cultivates critically
                thinking individuals who want to learn and to grow into co-operative, loving, confident, concerned
                global citizens.
            </div>
        </div>
    </div> <!--end of home parallax -->
</section>



<section class="home-photo-gallery">
    <div class="container">
<h4>Gallery</h4>
<div id="wowslider-container1">

<div class="ws_images"><ul>
<li><img src="{{asset('images/gallary/1.JPG')}}" alt="Cascade in the jungle" title="" id="wows1_0"/></li>
<li><img src="{{asset('images/gallary/2.JPG')}}" alt="Cityscape, Thailand" title="" id="wows1_1"/></li>
<li><img src="{{asset('images/gallary/3.JPG')}}" alt="Hermit crab" title="" id="wows1_2"/></li>
<li><img src="{{asset('images/gallary/4.JPG')}}" alt="Islands" title="" id="wows1_3"/></li>
<li><img src="{{asset('images/gallary/5.JPG')}}" alt="Hidden lizard" title="" id="wows1_4"/></li>
<li><img src="{{asset('images/gallary/6.JPG')}}" alt="Amazing nature" title="" id="wows1_5"/></li>
<li><img src="{{asset('images/gallary/7.JPG')}}" alt="Stream" title="" id="wows1_6"/></li>
<li><img src="{{asset('images/gallary/8.JPG')}}" alt="Fantastic view" title="" id="wows1_7"/></li>
</ul>
</div>

<div class="ws_thumbs">

<a href="#" title=""><img src="{{asset('images/gallary/1.JPG')}}" /> </a>
<a href="#" title=""><img src="{{asset('images/gallary/2.JPG')}}" alt="Cityscape, Thailand" /> </a>
<a href="#" title=""><img src="{{asset('images/gallary/3.JPG')}}" alt="Hermit crab" /> </a>
<a href="#" title=""><img src="{{asset('images/gallary/4.JPG')}}" alt="Islands" /> </a>
<a href="#" title=""><img src="{{asset('images/gallary/5.JPG')}}" alt="Hidden lizard" /> </a>
<a href="#" title=""><img src="{{asset('images/gallary/6.JPG')}}" alt="Amazing nature" /> </a>
<a href="#" title=""><img src="{{asset('images/gallary/7.JPG')}}" alt="Stream" /> </a>
<a href="#" title=""><img src="{{asset('images/gallary/8.JPG')}}" alt="Fantastic view" /> </a>

</div>
<div class="ws_shadow"></div>
</div>
<script type="text/javascript" src="https://wowslider.com/images/demo/wowslider.js"></script>
<script type="text/javascript" src="https://wowslider.com/sliders/demo-70/engine1/script.js"></script>
<!-- End WOW Slider.com BODY section -->
</div>
</section>

<section class="testinomial">
<div class="container">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
      <ol class="carousel-indicators">
          @foreach($testinomial as $test)
              <li data-target="#myCarousel" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
          @endforeach
      </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
@foreach($testinomial as $key => $test)
    @if($key==0)
      <div class="item active">
<div class="container">
<div class="row">
<div class="col-sm-3 col-sm-offset-4 margin-left">
<img src=" {{$test->image}}">
</div>
</div>
</div>
               <div class="carousel-captio">
          <h3>{{$test->name}}</h3>
          <span>{{$test->desg}}</span>
          <p>{{$test->body}}</p>
        </div>
      </div>
@elseif($key>0)
                <div class="item ">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-4 margin-left">
                                <img src=" {{$test->image}}">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-captio">
                        <h3>{{$test->name}}</h3>
                        <span>{{$test->desg}}</span>
                        <p>{{$test->body}}</p>
                    </div>
                </div>
            @endif

                @endforeach
    </div>






    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>

  </div>
</div>
</section>


<div style="clear:both;"></div>

<div class="girl-con">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <img src="{{asset('images/gallary/11.JPG')}}" width="100%" alt="nepalese in bangalore">
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="title-course">Who We Are ?</div>
                <div class="girl-con-body"><p>Spangle School was established in 2050 BS in Kalikasthan,Kathmandu with 78 students and 25 staff
                        members. Initially, classes from Nursery to grade III were offered. Today, it has become a
                        full-fledged higher secondary school running classes from grade one to twelve. Upon completion
                        of the construction of its own school complex at Kalikasthan, which is 1.5 kilometers away from
                        old Bus Park, it shifted all classes to the new complex. </p>

                    <p>The school has excellent infrastructure supported by state-of-the-art facilities. It has modern
                        academic buildings with spacious classrooms, laboratories, conference hall, hostel buildings,
                        administrative offices, a cafeteria and spacious playing grounds for games and sports such as
                        football, basketball, etc. There are separate buildings and rooms for extra-curricular
                        activities such as gymnastics, judo, keyboard, percussion, guitar, violin, sitar, etc.</p></div>
            </div>
        </div>
    </div>
</div>




