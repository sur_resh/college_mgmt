<section class="school-admission">


</section>
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-234" class="post-234 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Admission Procedure</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <div class="row">
                                <div class="container">
                                    <div class="col-sm-8 admission">
                                        <h4 class="admission"><strong>Secondary level Admission Requirements</strong></h4>
                                        <p>Admissions are announced immediately after the completion of SLC examinations each year. Prospective candidates are required to collect admission forms and sit for an entrance test.</p>
                                        <p>Application forms are given out during post SLC period and selections are assessed in English, Science and Mathematics in the Entrance Examination.</p>
                                        <h5 class="admission">All applications must submit the following with the application form:</h5>
                                        <ul class="admission">
                                            <li>Duly completed application form  together with 2 color passport-sized photographs.</li>
                                            <li>Certified copies of all relevant academic/ professional qualification and transcripts.</li>
                                            <li>Application processing free of NRs.1000/-</li>
                                            <li>2 pp size photographs</li>
                                            <li>1 copy of the Birth Certificate</li>
                                            <li>School Transfer Certificate</li>
                                            <li>Photocopy of the Last school Results</li>
                                            <li>Filled Application Form</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->

                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>
            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>


