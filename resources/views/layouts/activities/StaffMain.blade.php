<section class="school-staff">


</section>
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-167" class="post-167 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Staff</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <p>Galaxy has a highly qualified and competent teaching staff. The teachers help to promote the spiritual, moral, social, cultural, mental and physical development of pupils.</p>
                            <p>The different sections of the school are under the direct control of supervisors and head teachers who in turn, report to the Principal.</p>
                            <p>Co-curricular activities are managed by professionals who are experts in their own fields.</p>
                            <p>The best part of Galaxy is the intellectual growth that comes from rigorous classes taught by faculty members who are experts in their field.</p>
                        </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->

                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>
            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>


