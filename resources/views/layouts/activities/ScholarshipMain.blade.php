<section class="school-scholarship">


</section>
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-217" class="post-217 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Scholarship</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <div class="row">

                                    <div class="col-sm-12 hseb-scholar">
                                        <p>Merit Based Scholarship Award</p>
                                        <table>
                                            <tbody><tr>
                                                <th>
                                                    <h4>Scholarship</h4>
                                                </th>
                                                <th>
                                                    <h4>Requirement</h4>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    100% scholarship
                                                </td>
                                                <td class="color">
                                                    <ul>
                                                        <li>Female student with 80+ and Top 5 female candidate in College Entrance Test</li>
                                                        <li>Male student with 80+  and Top 5 male candidate in College Entrance Test</li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    50% scholarship
                                                </td>
                                                <td class="color">
                                                    <ul class="hseb-scholar">
                                                        <li>Female student with 75+ and Top 10 female candidate in College Entrance Test</li>
                                                        <li">Male student with 25+  and Top 10 male candidate in College Entrance Test
                                                        </li"></ul>
                                                </td>
                                            </tr>
                                            </tbody></table>
                                        <p>Primary &amp; Secondary Level Scholarship</p>
                                        <table>
                                            <tbody><tr>
                                                <th>
                                                    <h4>Scholarship</h4>
                                                </th>
                                                <th>
                                                    <h4>Requirement</h4>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    100% scholarship
                                                </td>
                                                <td class="color">
                                                    <ul>
                                                        <li>Female student scoring Highest percentage in each individual class</li>
                                                        <li>Male student scoring Highest percentage in each individual class </li>
                                                    </ul>

                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    50% scholarship
                                                </td>
                                                <td class="color">
                                                    <ul class="hseb-scholar">
                                                        <li>Female student scoring Second Highest percentage in each individual class</li>
                                                        <li>Male student scoring Second Highest percentage in each individual class </li>
                                                        <ul>
                                                        </ul></ul></td>
                                            </tr>

                                            </tbody></table>
                                    </div>

                                <div>
                                </div><!-- .entry-content -->
                            </div><!-- .entry-content-wrapper -->

                            <footer class="entry-footer">
                            </footer><!-- .entry-footer -->
                        </div></div></article>
            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>


