<section class="school-admission-entrance">


</section>
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-215" class="post-215 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Admission Entrance Notice</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <div class="row">
                                <div class="entrance">
                                    <p class="entranc">Admission Timeline</p>
                                    <h5 class="entrance">
                                        Grade 1: February – March<p></p>
                                        <p>HSEB Grade XI: June – July</p>
                                        <p>IBDP Year 1: April – May<br>
                                        </p></h5>
                                    <h4>Parents and guardians who wish to enroll their child at our School should follow these steps:</h4>
                                    <p>Step 1: Contact the Admission’s Office to find out about the academic program and arrange an appointment and school tour.</p>
                                    <p>Step 2: Collect the application package, including the application form, from the school reception desk. The application form is also available on the school website at www.nepgeeks.edu.np.</p>
                                    <p>Step 3: Fill out the application form and submit it to the Admission’s Office. The Admission’s Office will set a date and time for a placement assessment and an interview. Both the parents or guardians and the student shall be interviewed.</p>
                                    <p>Step 4: Parents or guardians and students will be notified by telephone or email if the student has been accepted.</p>
                                    <p>Step 5: Upon admission, parents or guardians and students will receive an acceptance letter and a voucher to make a deposit that covers the admission fee, security deposit, and annual charge. This deposit confirms the admission of the student at our School. If a deposit is not made by the given date, a student from the waiting list will be called.</p>
                                    <p>Step 7: Parents or guardians are required to attend a parent orientation meeting where they will be provided with a Parent’s Student’s Handbook and other necessary documents.</p>
                                    <p>Entrance Test and Interview</p>
                                    <p>Grade 1: Placement assessment, followed by a brief interview with the parents.</p>
                                    <p>IBDP and HSEB: Written test in mathematics and English, followed by a short interview with the student.</p>
                                </div>
                            </div>
                        </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->

                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>
            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>


