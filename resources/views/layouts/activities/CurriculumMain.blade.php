<section class="school-curriculum">


</section>
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-165" class="post-165 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Curriculum</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <p>The school aims to introduce a broader and more balanced curriculum to enable all pupils to develop the qualities and skills needed in adult life.</p>
                            <p>The school provides support to the students who are excellent at Science and Technology, Social services and Culture &amp; Sports by developing various skills and imparting knowledge.</p>
                            <p>It prepares students for the S.L.C. Examinations and the Higher Secondary Examinations, for the purpose of which the school has an excellent team of well-educated and experienced teachers. In other sections of the school, the school follows its own curriculum which has been designed to meet the needs of students.</p>
                            <p>A common curriculum consisting of Moral Education, English, Nepali, Mathematics, Science, Environmental Science, Social Studies, Art &amp; Craft and Computer Studies is taught in all the other classes.</p>
                            <p>Pre-School consists of two levels: Prep I and Prep II. The minimum age for admission into Prep I is 3 + years. For Prep II it is 4 + years. Informal education is imparted to students in the Pre-School.</p>
                            <p>The Children are made to do a lot of oral work, indulge in various kinds of activities, play games, do simple practicals, go for excursions, and observe different things and phenomenon. Teachers use several audio-visual aids in order to demonstrate the lessons.</p>
                            <p>The aim of the curriculum at this stage is to make children creative and innovative. To develop good toilet habits, to encourage children to eat healthy foods and to make them aware of the benefits of good health. The Kindergarten school is of one year. The minimum age for admission to this class is 5+ years. Formal education stars from this class.</p>
                        </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->

                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>
            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>


