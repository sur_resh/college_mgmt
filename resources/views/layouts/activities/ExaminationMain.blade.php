<section class="school-exam">


</section>
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-254" class="post-254 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Examination</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <p>Examination is held at the end of every week.A test is conducted on any major subject. The syllablus for the test is only one or two chapters. This test is to ascertain whether a student can understand the lessons taught in class.</p>
                            <p>Mid-term exams are held in the middle of each term. Mid-term exams cover the lessons taught in 40 working days. They are held thrice a year and each exam is of 90 mins. In this round of exams, students are tested on all major subjects. These tests are to ascertain whether a student revises the lessons taught and works hard to achieve better grades.</p>
                            <p>Terminal examinations are held at the end of each term. There are three rounds of terminal exams. Each exams carries 100 marks. These exams cover the lessons taught in 80 working days. These exams are conducted on all major subjects to ensure that the students can handle full-length examinations or board examinations.</p>
                        </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->

                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>
            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>


