<section class="school-attendence">


</section>
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-250" class="post-250 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Attendance</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <p>Students are expected to attend school on all working days and be punctual. The school does not, as a rule, approve of any leave during the session except in very exceptional circumstances like serious illness (supported by medical certificate), marriage of a real brother or sister or any unfortunate incident like death of a family member.</p>
                            <p>Children who absent themselves without valid reasons and prior sanction will be fined @Rs.50/- per day upto 7 days after which their names will be struck off the rolls and they can be re-admitted only if their re-admission is approved by the Principal. Students who report late at the school will either be sent back or fined.</p>
                            <p>It may be appreciated that for proper education continuity, regularity and punctuality are essential. The measures mentioned above are only to ensure that students do not deny themselves the fullest benefit of education.</p>
                        </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->

                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>
            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>


