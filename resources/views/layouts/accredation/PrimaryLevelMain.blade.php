<section class="school-Primarylevel">


</section>
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-278" class="post-278 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Primary Level</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <p>Primary education and elementary education is typically the first stage of compulsory education, coming between early childhood education and secondary education. Primary education usually takes place in a primary school or elementary school. In some countries, primary education is followed by middle school, an educational stage which exists in some countries, and takes place between primary school and high school.<br>
                                Our middle school curriculum honors authentic inquiry, experimentation and learning in a spirit of discovery.  Our interactive, project-oriented classroom environments encourage student creativity and academic excellence, while building effective problem-solving skills. Students have ready access to academic enrichment and support and an inviting range of opportunities for social and artistic enrichment, service learning and sports.</p>
                            <p>Our middle school students grow to understand themselves and one another as people, as learners and as members of a larger community.</p>
                        </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->

                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>
            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>


