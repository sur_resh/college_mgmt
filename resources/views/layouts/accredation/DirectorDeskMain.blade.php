
<div class="container">
    <div class="row">

        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-258" class="post-258 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Chairman Desk’s</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <div class="row">
                                <div class="col-sm-4">
                                </div>
                                <div class="col-sm-4 principle">
                                    <img src="/public/images/IMG_2575.JPG" alt="" width="100%" height="auto" class="aligncenter size-full wp-image-314" srcset="/images/IMG_2575.JPG" sizes="(max-width: 443px) 100vw, 443px"><p></p>
                                    
                                </div>
                                <div class="col-sm-4">
                                </div>
                            </div>
                            <div class="entry-content">
                            <p>I am overwhelmed with joy as we have turned 25 years old. On this very day I would like to remem-
ber the first founder chairman of the Spangle boarding high school, Mr. Gopal Das Karmacharya who

gave birth to this institution and took care for several years. He had seen the future and destination of
this institution. Because of his contribution and dreams, we are here to observe its silver jubilee. At this
moment, I would like to thank, honour and remember former princiipal, founders, students and valuable
parents and guardians for their remarkable contribution without whcih this day of success wouldn't be
possible.</P>
<p>Now, The spangle boarding high school has grown up and has been running smoothly with the clear
vision of sweet fruiting and flowering. Considering all the achievements made so far, we have every good
reason to feel, sense of pride on this day. As this is the institution run by professional teachers, there is
no any confusion regarding its destination. We are committed to provide quality education as per the
demand of this century.</P>

<p>Change is unavoidable, we keep strive to do so, as the digital and technological revolution is chang-
ing everything. We have planned to run class 11 and 12 according to new curriculam prepared by

curricular planning & development centre. For this, the process & infrastructure development is started.
Still we need hard work and effort, we need to sharpen our tools and skills according to time and
context.</P>
<p>I assure quality education and its implication as students and teachers are the spirit & life force of this
institution. I would like to honour all the parents & guardians without whom this success is impossible.
Similarly, I would like to express my sincere appreciation to the co-editors & Board of editors for
bearing the responsibility of the prodcuction of this magazine and all the students & teachers who made
the 25th anniversary celebrations a huge success.</P><br>

<strong>Mr. Chhatra Mani Katuwal<br>
Chairman</strong>
</p>
                            </div>
                        </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->

                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>
            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>


