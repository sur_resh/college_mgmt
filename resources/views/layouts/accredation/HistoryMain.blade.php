<section class="school-History">


</section>
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-286" class="post-286 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">History</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <div class="row">
                                <div class="col-sm-4">
                                </div>
                                <div class="col-sm-4 principle">
                                    <img src="/public/images/IMG_2575.JPG" alt="" width="100%" height="250" class="aligncenter size-full wp-image-314" srcset="/images/IMG_2575.JPG" sizes="(max-width: 443px) 100vw, 443px"><p></p>
                                    
                                </div>
                                <div class="col-sm-4">
                                </div>
                            </div>
                            <div class="entry-content">
                            <p>Our School is renowned as pioneers and leaders in the field of education committed to educate and transform by inspiring. Since it started in the year 1981 as a small school in a rented house for 65 day scholars it has inspired thousands of young minds, nurturing talents and helping students realize their true potential. Today, it is renowned for its academic excellence and as the largest private school, spread amidst 350 ropanis of land in a serene ambience with matchless facilities, well trained experienced faculty who foster and help achieve excellence. It continues to expand and grow touching the lives of young minds, inspiring, encouraging personal and academic growth through collaborative and student centered methods in a dynamic environment.<br>
                                A growing appreciation and demand for quality education and the remarkable personal touch that it offered saw a rapid expansion. The year 1990, saw the first batch of SLC students who excelled in academics; each passing year brought accolades, several rewarding and precious moments. During the 1990s, we undertook the ambitious program to build one of the largest and sophisticated school premises with modern amenities to impart premium education. The year 1995, saw the start of the construction in 350 ropanies of land at Amarxin– the serene and idyllic lush green location matched with the desire to provide an ideal environment for a truly fulfilling learning experience. The features that keep us at par institutions at a global level are infrastructure, an array of innovations in the field of education, child-centered mode of education, modern teaching aids like over head projectors/ AV-gadgets, multi -media systems, hi-tech computer labs, student support services and the assimilation of extra-curricular activities as an integral part of daily activities. To reach the pinnacle of success, we proceed on an endeavor directed to making students secure and efficient at a global level.</p>
                        </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->
                    </div>
                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>
            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>


