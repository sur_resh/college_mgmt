<section class="school-secondarylevel">


</section>
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-212" class="post-212 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Secondary Level</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <p>The secondary school program consists of students of grades IX and X and the Nepal Government’s HSEB Program. Like other programs of the our School, the students of secondary school also experience a holistic learning experience courtesy to the teaching learning pedagogy. Our students are involved in numerous and diverse activities that aims at encouraging creativity, independent critical thinking and international mindedness.</p>
                            <p>Our HSEB Program</p>
                            <p> HSEB (10+2) Program is a rigorous two-year higher secondary program that enables motivated students to become well-rounded, dynamic people.</p>
                            <p>We began the 10+2 program in 2009. Our graduates are prepared not only to excel in examinations but also to deal with real-life scenarios – the hallmark of our School’s philosophy.</p>
                            <p>Our program’s uniqueness lies in its focus on creativity and critical thinking through curricular, co-curricular and extra-curricular activities..</p>
                        </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->

                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>
            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>


