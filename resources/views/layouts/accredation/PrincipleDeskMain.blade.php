<section class="school-Principle">


</section>
<div class="container">
    <div class="row">

        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-258" class="post-258 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Principle Desk’s</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <div class="row">
                                <div class="col-sm-4">
                                </div>
                                <div class="col-sm-4 principle">
                                    <img src="/public/images/IMG_2557.JPG" alt="" width="100%" height="auto" class="aligncenter size-full wp-image-314" srcset="/images/IMG_2557.JPG" sizes="(max-width: 443px) 100vw, 443px"><p></p>
                                                                    </div>
                                <div class="col-sm-4">
                                </div>
                            </div>
                            <div class="entry-content">
                            <p>I am delighted to write few words about the school on the occasion of celebrating ‘Silver Jubilee
Year’ as well as continuation of publishing the school souvenir ‘The Bloom’.
The Spangle Boarding High School is one of the reputed schools in Kathmandu Valley, established

by veteran educationists, experts and dynamic personalities in 2050 B.S. with an objective of impart-
ing quality education for all. The school has tried to set up a conducive environment where every

child gets equal opportunity to be involving on both co-curricular and extra-curricular activities. The
school has successfully completed its 24 years of sphere of life and heading to step on the threshold
of silver jubilee year. In this regard, the school wants to memorize all the scholastic achievements and
contributions of more than two decades and share it with reciprocated individualities.</p>
<p>The school’s genuine concern, since the beginning of establishment time to till the date, is to give

newness in academic field by introducing and implementing technology-based as well as scientif-
ic modern teaching learning methodology with an expectation that students can academically find

themselves wide discrepancies from other and enjoy all the benefits and privileges for their further
study. Such practical based teaching methodology has enriched the school’s activities to fostering
and strengthening the relation between the parents and school. This is only possible here due to well
experienced, dedicated and supporting teaching and non-teaching staff.</p>
<p>Besides academic activities, school has provided maximum facilities and platforms for the students to
take part in inter and intra curricular activities that have taught them a bit of grit in their eye, analyzing
situation, importance of essential skills, expressing confidently and instigating valued policy.
Our mission is to give our students the best opportunities and all round education that will ultimately
give more confidence to compete in the area of global market. Our vision is to produce innovative,
courageous, smart, confident and multifaceted talented citizens of our country who will be the leaders
of tomorrow for different sectors and make us always proud.</p>
<p>At last, I would like to extend my heartfelt gratitude to editorial board, teachers, students, parents,

well-wishers and school management committee for their enthusiastic support in publishing this sou-
venir in time.</p><br>

<strong>Mr. Dhan Pd. Angdembe (Subba)<br>
Principal</strong>
</p>
                            </div>
                            </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->

                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>
            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>


