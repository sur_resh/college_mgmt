<section class="school-Hseb">


</section>
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-280" class="post-280 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">HSEB</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <p>The Higher Secondary Education Board (HSEB) has been running the 10 + 2 system in the country in both English and Nepali medium in Management and Humanities except for Science which is available in English only.</p>
                            <div class="hseb">
                                <h4>Streams offered at +2</h4>
                                <ul>
                                    <li> Science</li>
                                    <li> Management</li>
                                    <li> Humanities</li>
                                </ul>
                                <h4>Curriculum</h4>
                                <p>Saipal follows the text books recommended by the HSEB.  Besides, we emphasize on ECAs/CCAs and additional life skills.  Divided into two years, Saipal provides HSEB Management, Science, and Humanities programmes which stress on the importance of broad educational background for accomplishing its objectives at the higher study level.  At present SAHSS offers the following courses at +2 levels.</p>
                                <h4>Evaluation</h4>
                                <p>SAHSS has its own system of internal evaluation for the assessment of the students. They must take terminal tests, show good performance and complete every home assignment. Students should mandatorily attend and pass the terminal and pre–board examinations. The external evaluations are set by the HSEB.</p>
                                <h4>Teaching Learning Methods &amp; Techniques</h4>
                                <h4>Lectures followed by discussion</h4>
                                <ul>
                                    <li>Book reading assignment/ Paper presentation</li>
                                    <li>Home assignment/ project work</li>
                                    <li>Individual counseling</li>
                                    <li>Psychological counseling</li>
                                    <li>Career guidance</li>
                                    <li>Use of ICT (Audio Video aids)</li>
                                    <li>Educational tours/Excursions</li>
                                    <li>Provision of visiting classes from renowned personalities</li>
                                    <li>Enrollment Procedures and Criteria</li>
                                    <p></p>
                                    <h4>Eligibility for +2 programmes: </h4>
                                    <p>At least second division in SLC exams or O level passed. Students must come with parents/guardians with equivalent transfer/ character certificates, mark sheet, and 2 recent pp sized photographs while applying for the admission.  Admission arrangements are discussed in detail with parents and it is carried out on the basis entrance test followed by interview. Students must pass SAHSS entrance test to be qualified. The final selection is decided by the selection committee of the college. Students must fill in a commitment form at the time of admission. Enrollment fee is charged only once for the entire course.</p>
                                </ul></div><!-- .entry-content -->
                        </div><!-- .entry-content-wrapper -->

                        <footer class="entry-footer">
                        </footer><!-- .entry-footer -->
                    </div></article>
            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>


