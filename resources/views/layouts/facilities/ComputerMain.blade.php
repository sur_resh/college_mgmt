<section class="school-computer">


</section>
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-210" class="post-210 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Computer Laboratories</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <!-- <div class="row">
                                <div class="col-sm-4">
                                </div>
                                <div class="col-sm-4 principle">
                            <img src="{{asset('/images/IMG_2436.JPG')}}" alt="" width="100%" height="auto" class="aligncenter size-full wp-image-314" srcset="{{asset('/images/IMG_2436.JPG')}}" sizes="(max-width: 443px) 100vw, 443px"><p></p>
                                                                    </div>
                                <div class="col-sm-4">
                                </div>
                            </div> -->
                            <div class="entry-content">
                            <p><a href="{{asset('/images/IMG_2436.JPG')}}"><img src="{{asset('/images/IMG_2436.JPG')}}" alt="" width="2384" height="472" class="aligncenter size-full wp-image-295"></a></p>

                            <p class="achivements">Computer Lab</p>
                            <p>St. George’s has five computer laboratories featuring over two hundred computer systems. The computers in the CBSE lab have been upgraded with appropriate software to enable students of classes XI and XII to compile and run programs in Java with net beans and C++ along with Flash-8 applications as per the syllabus. The Senior, Junior and Sub-Junior Computer Labs, on the other hand,enable students to learnVB.net, Corel Draw, Java, Adobe Flash, Kid pix, Logo, Paintbrush, MS-Office, etc.</p>
                            <p>In addition, the school has a computerized mathematics laboratory. It is an activity-centred, problem-solving</p>
                        </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->
                    </div>
                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>
            </section>


        </div>

    <div class="col-sm-4 left-widget">
        @include('admin-panel.SideBar')
    </div>
    </div>
</div>


