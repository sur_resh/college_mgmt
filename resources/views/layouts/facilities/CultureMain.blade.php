<section class="school-culture">


</section>
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-252" class="post-252 page type-page status-publish hentry">

                	<header class="entry-header">
                		<h1 class="entry-title">Cultural Activity</h1>	</header><!-- .entry-header -->

                    	<div class="entry-content-wrapper">
                		<div class="entry-content">
                            <!-- <div class="row">
                                <div class="col-sm-4">
                                </div>
                                <div class="col-sm-4 principle">
                                    <img src="/public/images/IMG_2008.JPG" alt="" width="100%" height="auto" class="aligncenter size-full wp-image-314" srcset="/images/IMG_2008.JPG" sizes="(max-width: 443px) 100vw, 443px"><p></p>
                                                                    </div>
                                <div class="col-sm-4">
                                </div>
                            </div> -->
                            <div class="entry-content">
                        <p><a href="{{asset('/images/IMG_2008.JPG')}}"><img src="{{asset('/images/IMG_2008.JPG')}}" alt="" width="2384" height="472" class="aligncenter size-full wp-image-295"></a></p>

                			<p>large number of Co-curricular activities are organized throughout the academic year. Important activities are:</p>
                <p>Dramatics ( English/Hindi/Nepali )<br>
                Public Speaking ( Declamation, Debate, Elocution, Extempore Speech, Story Telling &amp; Recitation )<br>
                Creative Writing ( Essay, Story &amp; Poetry )<br>
                Creative Work ( Magazine Designing, Wall Magazine, Bulletin Board, Flower Arrangement, Rangoli &amp; Salad Decoration )<br>
                Creative Art ( Painting, Drawing &amp; Sketching )<br>
                Quiz ( General Awareness, Current Affairs &amp; Situational Awareness )<br>
                Dance ( Classical &amp; Folk )<br>
                Music ( Classical &amp; Folk )</p>
                					</div><!-- .entry-content -->
                	</div><!-- .entry-content-wrapper -->
                    </div>
                	<footer class="entry-footer">
                			</footer><!-- .entry-footer -->
                </article>
            </section>


        </div>
        <div class="col-sm-4 left-widget">
                    @include('admin-panel.SideBar')
                </div>
    </div>
</div>


