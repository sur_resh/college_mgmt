<section class="school-sports">
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-262" class="post-262 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Sports</h1> </header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
    <p><img src="{{asset('/images/IMG_2515.JPG')}}" alt="" width="100%" height="auto" class="aligncenter size-full wp-image-314" srcset="{{asset('/images/IMG_2515.JPG')}}" sizes="(max-width: 443px) 100vw, 443px"><p></p>
</p>
                            
                            <p>Students are encouraged to play different sports and games under close supervision of trained Physical Education teachers and specialized coaches. Students are trained in the following discipline:</p>
                            <p>  -&gt;Athletics<br>
                                -&gt;Football<br>
                                -&gt;Cricket<br>
                                -&gt;Basketball<br>
                                -&gt;Badminton<br>
                                -&gt;Table Tennis<br>
                                -&gt;Volleyball</p>
                            <p>In addition, students are trained in martial arts like Karate, Wushu and Teakwondo. Classes are also conducted in Yoga under the close supervision of Yoga teachers.</p>
                        </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->

                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>
            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>

</section>



