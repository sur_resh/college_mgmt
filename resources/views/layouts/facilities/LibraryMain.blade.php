<section class="school-library">


</section>
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-288" class="post-288 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Library</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <p><a href="{{asset('/images/IMG_2436.JPG')}}"><img src="{{asset('/images/IMG_2436.JPG')}}" alt="" width="2384" height="472" class="aligncenter size-full wp-image-295"></a></p>
                            <p>We are passionate about  reading  and encourage students to go beyond  the prescribed curriculum expand their horizons, have stronger analytical and critical skills. libraries engage students in a  world of ideas. With a host of books to suit the taste every book lover;reading is promoted ardently by BAS.</p>
                        </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->

                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>

            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>


