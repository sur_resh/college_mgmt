<section class="school-lab-science">
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="science-lab-details">
                <article id="post-292" class="post-292 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Science Laborataries</h1>   </header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <!-- <div class="row">
                                <div class="col-sm-4">
                                </div>
                                <div class="col-sm-4 principle">
                                    <img src="{{asset('/images/IMG_2515.JPG')}}" alt="" width="100%" height="auto" class="aligncenter size-full wp-image-314" srcset="{{asset('/images/IMG_2515.JPG')}}" sizes="(max-width: 443px) 100vw, 443px"><p></p>
                                                                    </div>
                                <div class="col-sm-4">
                                </div>
                            </div> -->
                            <div class="entry-content">
                                <p><img src="{{asset('/images/IMG_2515.JPG')}}" alt="" width="100%" height="auto" class="aligncenter size-full wp-image-314" srcset="{{asset('/images/IMG_2515.JPG')}}" sizes="(max-width: 443px) 100vw, 443px"><p></p>
</p>
                            <p>All the laboratories are well-stocked with sufficient chemicals, specimens and instruments so that every student gets the opportunity to carry out the experiments individually. Well-qualified and experienced teachers guide the students to explore the world of science through a meticulously planned schedule of practical classes. Moreover, the safety of every child while handling the chemicals and instruments is duly taken care off.St. George’s has well-equipped laboratories for imparting practical knowledge in Physics, Chemistry and Biology. There are two laboratories for each of the science subjects, one for classes IX and X, and the other for classes XI and XII. The aim of having separate labs is to provide ample time and adequate equipment for every student to perform the experiments as per the CBSE guidelines.</p>
                            <p>All the laboratories are well-stocked with sufficient chemicals, specimens and instruments so that every student gets the opportunity to carry out the experiments individually. Well-qualified and experienced teachers guide the students to explore the world of science through a meticulously planned schedule of practical classes. Moreover, the safety of every child while handling the chemicals and instruments is duly taken care off.</p>
                        </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->
                    </div>
                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>

            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>

</section>



