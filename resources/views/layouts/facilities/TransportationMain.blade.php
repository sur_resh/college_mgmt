<section class="school-Transporatation">


</section>
<div class="container">
    <div class="row">


        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-260" class="post-260 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Transportation</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <!-- <div class="row">
                                <div class="col-sm-4">
                                </div>
                                <div class="col-sm-4 principle">
                                    <img src="/public/images/IMG_2008.jpg" alt="" width="100%" height="auto" class="aligncenter size-full wp-image-314" srcset="/images/IMG_2008.jpg" sizes="(max-width: 443px) 100vw, 443px"><p></p>
                                                                    </div>
                                <div class="col-sm-4">
                                </div>
                            </div> -->
                            <div class="entry-content">
                                <p>
                                    <img src="/public/images/IMG_2008.jpg" alt="" width="100%" height="auto" class="aligncenter size-full wp-image-314" srcset="/images/IMG_2008.jpg" sizes="(max-width: 443px) 100vw, 443px">
                                 </p>
</p>
                            <p>School buses are available for the students of all classes, but in view of the limited number of seats, preference is given to small children and to those students who have to travel long distance.</p>
                            <p>Bus fees are payable in advance along with the tuition fees by 15th of the billing month.</p>
                            <p>One month’s notice must be given for withdrawal from the bus service or pay one month’s fee in lieu of the notice period.</p>
                            <p>The bus pass should be renewed every month and shown to the Bus Conductor on demand.</p>
                            <p>All users of the School bus facility must observe the rules prescribed for them. The facility can be withdrawn from those who violate the rules and commit offences like smoking, fighting, ill treat other students, showing disrespect to the staff, not observing safety rules or not maintaining good order and discipline in the bus.</p>
                            <p>The school bus covers a large number of localities of Kathmandu but it is not possible to cater for all areas. If the parents desire to use the bus service for their wards they are advised to confirm from the school office whether the area in which they live is serviced by the school buses.</p>
                        </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->
                     </div>
                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>
                </article>
            </section>


        </div>
        <div class="col-sm-4 left-widget">
            @include('admin-panel.SideBar')
        </div>
    </div>
</div>


