@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="container card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/home">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item">
						<a href="/home/gallery">gallery</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">create</li>
				</ul>
			</div>
		</div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
		<div class="card-body card-block">
			<form action="/home/gallery/create" method="POST" class="form-horizontal" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="name" class=" form-control-label">Name</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="name" id="name" name="name" placeholder="Enter Name..." class="form-control">

					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="image" class=" form-control-label">Image input</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="file" id="image" accept="image/png, image/jpg, image/jpeg, video/mp4" name="image" class="form-control-file">
					</div>
				</div>
				<button type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Submit
				</button>
				<button type="reset" class="btn btn-danger btn-sm">
					<i class="fa fa-ban"></i> Reset
				</button>
			</form>
		</div>
	</div>
</div>




@endsection