@extends('layouts.mainpage.app')

@section('content')


<div id="NoticeModal" class="modal fade in" role="dialog" aria-hidden="false" style="display: block;">
    <div class="modal-dialog ">
        <!-- Modal content-->
        <?php $count=1; ?>
    @foreach($modals as $m)
      
        <div class="modal-content" style="background-color:whitesmoke">
            <div class="modal-header">
                <h4 class="modal-title text-center">{{$m->name}}</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
            	
                 <p>
                    </p><center><img class="img-responsive" style="width:100%;height:auto;" src="{{ asset('/images/'.$m->photo )}}"></center>
                <p></p> 
                <p class="text-center">
                    </p><p style="text-align: justify;"><br></p>

                <p></p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" data-dismiss="modal">Close</button>
                
            </div>
        </div>
    <?php $count ++;
    if($count > 1)
        break;
    ?>
      @endforeach
    </div>
</div>


<section class="home-slider owl-carousel">

@foreach($sliders as $slide)
<div class="slider-item" style="background-image:url(/images/{{ $slide->photo}});">
<div class="overlay"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
<div class="col-md-8 text-center ftco-animate">
<!-- <h1 class="mb-4">Kids Are The Best <span>Explorers In The World</span></h1>
 <p><a href="#" class="btn btn-secondary px-4 py-3 mt-3">Read More</a></p> -->
 </div> 
 </div>
 </div> 
</div>
@endforeach

<!-- <div class="slider-item" style="background-image:url(images/bg_2.jpg);">
<div class="overlay"></div>
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
<div class="col-md-8 text-center ftco-animate">
<h1 class="mb-4">Perfect Learned<span> For Your Child</span></h1>
<p><a href="#" class="btn btn-secondary px-4 py-3 mt-3">Read More</a></p>
</div>
</div>
</div>
</div> -->
</section>



<section class="ftco-services ftco-no-pb">
<div class="container-wrap">
<div class="row no-gutters">
<div class="col-md-3 d-flex services align-self-stretch pb-4 px-4 ftco-animate bg-primary">
<div class="media block-6 d-block text-center">
<div class="icon d-flex justify-content-center align-items-center">
<span class="flaticon-teacher"></span>
</div>
<div class="media-body p-2 mt-3">
<h3 class="heading">Experienced Teachers</h3>
<p>Our school provides quality education through qualified graduated and post graduated experienced teachers.</p>
</div>
</div>
</div>
<div class="col-md-3 d-flex services align-self-stretch pb-4 px-4 ftco-animate bg-tertiary">
<div class="media block-6 d-block text-center">
<div class="icon d-flex justify-content-center align-items-center">
<span class="flaticon-reading"></span>
</div>
<div class="media-body p-2 mt-3">
<h3 class="heading">Special Morning Class </h3>
<p>We provide special morning classes to SEE appearing students along with day boarders facility.</p>
</div>
</div>
</div>
<div class="col-md-3 d-flex services align-self-stretch pb-4 px-4 ftco-animate bg-fifth">
<div class="media block-6 d-block text-center">
<div class="icon d-flex justify-content-center align-items-center">
<span class="flaticon-books"></span>
</div>
<div class="media-body p-2 mt-3">
<h3 class="heading">Lab &amp; Library</h3>
<p>Well equipped lab and library with latest equipments and updated books.</p>
</div>
</div>
</div>
<div class="col-md-3 d-flex services align-self-stretch pb-4 px-4 ftco-animate bg-quarternary">
<div class="media block-6 d-block text-center">
<div class="icon d-flex justify-content-center align-items-center">
<span class="flaticon-diploma"></span>
</div>
<div class="media-body p-2 mt-3">
<h3 class="heading">Awards & Scholarship</h3>
<p>Yearly scholarship scheme to deserving students and awards to winner of various activities.</p>
</div>
</div>
</div>
</div>
</div>
</section>



<section class="ftco-section ftco-no-pt ftc-no-pb">
<div class="container">
<div class="row">
<div class="col-md-5 order-md-last wrap-about py-5 wrap-about bg-light">
<div class="text px-4 ftco-animate">
<h2 class="mb-4">Welcome to Spangle School</h2>
<p>The spangle boarding high school was established in 2050 BS and run and taught by professional experienced teachers team. For the sake of changing young minds into creation and develop a well cultivated, far sighted energetic and ambitious human being respired for this present world. It is situated at the heart of Kathmandu valley which boarders  dillbazar  ghattekulo  maitidevi and new plaza and putalisadak. As an academic institution concerned with providing a platform for over all development of children. 
</p>
<p>It is system based organization. The spangle boarding high school has formed family environment to make learning easier where corporal punishment is strictly prohibited. It is running its 258 years of establishment has launched several child friendly programs. We invite students to admit and encourage learning as a zest which acquires all round capabilities. </p>
<p> 
<div class="form-group">        
 <div class="col-md-6 col-md-offset-4">
             <button type="submit" a href="#"  class="btn btn-primary">
                   Read More
              </button>
</div>
</div>
</p>
</div>
</div>
<div class="col-md-7 wrap-about py-5 pr-md-4 ftco-animate">
<h2 class="mb-4">"Children are the future of the Nation,Our motto is well cultured quality education"</h2>

<div class="row mt-5">
<div class="col-lg-6">
<div class="services-2 d-flex">
<div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-security"></span></div>
<div class="text">
<h3>Students Alumni</h3>
<p>In addition to education and career opportunities, alumni associations offer a range of exclusive training to students. Some offer student scholarship,
 while some offer volunteering. </p>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="services-2 d-flex">
<div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-reading"></span></div>
<div class="text">
<h3>Evaluation System</h3>
<p><ul>
	<li>Class/Unit test/ Project works</li>
	<li>Terminal Examination</li>
	<li>Board Examination</li>
</ul></p>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="services-2 d-flex">
<div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-jigsaw"></span></div>
<div class="text">
<h3>Spangle Club</h3>
<p>It is exclusively meant for generating the skills of the students by performing various activities such as social works, publishing the magazines, etc.</p>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="services-2 d-flex">
<div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-kids"></span></div>
<div class="text">
<h3>Extra Activities</h3>
<p>Students are taken on excursion, historical visit and educational tours to various places in and the outskirts of the valley.</p>
</div>
</div>
</div>
<!-- <div class="col-lg-6">
<div class="services-2 d-flex">
<div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-diploma"></span></div>
<div class="text">
<h3>Scholarship Awards</h3>
<p>Scholarship awards exempt only tuition fees, which are allocated to few deserving students and such scholarship, shall be for a one academic year only.</p> 
<p>Student’s scholarship may be renewed on academic performance and general conduct of students.</p>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="services-2 d-flex">
<div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-education"></span></div>
<div class="text">
<h3>Code Of Conduct</h3>
<p>The students are required to follow the code of conduct maintained by school. Irresponsible and impolite behavior will be considered as an act of indiscipline. Vandalism and serious mis conducts will be sufficient reason for expulsion. </p>
<p>Students are required to attend the classes and functions in clean and smart dresses. </p>
</div>
</div>
</div>
 -->

</div>
</div>
</div>
</div>
</section>
<section class="ftco-intro" style="background-image: url({{asset('images/IMG_2795.jpg')}});" data-stellar-background-ratio="0.5">
<div class="overlay"></div>
<div class="container">
<div class="row">
<div class="col-md-9">
<h2>Our School Team Members </h2>
<p class="mb-0">We Offer a quality education in providing a bright future.</p>
</div>

</div>
</div>
</section>
<section class="ftco-section ftco-no-pb">
<div class="container">
<div class="row justify-content-center mb-5 pb-2">
<div class="col-md-8 text-center heading-section ftco-animate">
<h2 class="mb-4"><span>Dedicated</span> Teachers</h2>
<p>Team of highly qualified and expeienced teachers dedicated for better quality education.</p>
</div>
</div>
<div class="row">

<div class="col-md-6 col-lg-3 ftco-animate">
<div class="staff">
<div class="img-wrap d-flex align-items-stretch">
<div class="img align-self-stretch" style="background-image: url({{asset('images/IMG_2575.JPG')}});"></div>
</div>
<div class="text pt-3 text-center">
<h3>Mr.Chhatramani Katuwal</h3>
<span class="position mb-2">Chairman</span>
<div class="faded">

<ul class="ftco-social text-center">
<li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
<li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
<li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
<li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3 ftco-animate">
<div class="staff">
<div class="img-wrap d-flex align-items-stretch">
<div class="img align-self-stretch" style="background-image: url({{asset('images/IMG_2557.JPG')}});"></div>
</div>
<div class="text pt-3 text-center">
<h3>Mr.Dhan Prasad Subba</h3>
<span class="position mb-2">Principle</span>
<div class="faded">

<ul class="ftco-social text-center">
<li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
<li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
<li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
<li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3 ftco-animate">
<div class="staff">
<div class="img-wrap d-flex align-items-stretch">
<div class="img align-self-stretch" style="background-image: url({{asset('images/IMG_2578.JPG')}});"></div>
</div>
<div class="text pt-3 text-center">
<h3>Mrs.Nirmala Shrestha</h3>
<span class="position mb-2">Primary In-charge</span>
<div class="faded">

<ul class="ftco-social text-center">
<li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
<li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
<li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
<li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3 ftco-animate">
<div class="staff">
<div class="img-wrap d-flex align-items-stretch">
<div class="img align-self-stretch" style="background-image: url({{asset('images/rakesh.jpg')}});"></div>
</div>
<div class="text pt-3 text-center">
<h3>Rakesh Bhujel</h3>
<span class="position mb-2">Incharge</span>
<div class="faded">

<ul class="ftco-social text-center">
<li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
<li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
<li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
<li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="ftco-section courses">
<div class="container">
<div class="row justify-content-center mb-5 pb-2">
<div class="col-md-8 text-center heading-section ftco-animate">
<h2 class="mb-4"><span>Extra</span> Classes</h2>
<p>Apart from regular classes, we offer extra classes that enchance student's computer skills, analytical thinking and help them recognize their hidden talents. </p>
</div>
</div>
<div class="row">
<div class="col-md-6 course d-lg-flex ftco-animate">
<div class="img" style="background-image: url({{asset('/images/IMG_2475.JPG')}});"></div>
<div class="text bg-light p-4">
<h3><a href="#">Computer Class</a></h3>

<p>Our computer classes help students to get familiarized with new technology and explore into digital world.</p>
</div>
</div>
<div class="col-md-6 course d-lg-flex ftco-animate">
<div class="img" style="background-image: url({{asset('/images/IMG_2210.JPG')}});"></div>
<div class="text bg-light p-4">
<h3><a href="#">Language Class</a></h3>

<p>Students have options to learn foreign language based on their interest and choice.</p>
</div>
</div>
<div class="col-md-6 course d-lg-flex ftco-animate">
<div class="img" style="background-image: url({{asset('/images/IMG_1685.JPG')}});"></div>
<div class="text bg-light p-4">
<h3><a href="#">Music & Dance Class</a></h3>

<p>To introduce students with joy of music, we encourage them to learn musical instruments from our qualified music teachers.</p>
</div>
</div>
<div class="col-md-6 course d-lg-flex ftco-animate">
<div class="img" style="background-image: url({{asset('/images/IMG_2501.JPG')}});"></div>
<div class="text bg-light p-4">
<h3><a href="#">Laboratory Class</a></h3>

<p>Well equipped safe laboratory where students perform various experiments under constant supervision.</p>
</div>
</div>
</div>
</div>
</section>
<section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url({{asset('images/bg_4.jpg')}});">
<div class="icon mb-4 d-flex align-items-center justify-content-center">
<div class="container">
<div class="row justify-content-center mb-5 pb-2">
<div class="col-md-8 text-center heading-section heading-section-black ftco-animate">
<h2 class="mb-4"><span>25 Years of</span> Experience</h2>
<p>Conducive environment where every child gets equal opportunity to be involving on both co-curricular and extra-curricular activities.</p>
</div>
</div>
<div class="row d-md-flex align-items-center justify-content-center">
<div class="col-lg-10">
<div class="row d-md-flex align-items-center">
<div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
<div class="block-18">
<div class="icon"><span class="flaticon-doctor"></span></div>
<div class="text">
<strong class="number" data-number="30">0</strong>
<span>Experienced Teachers</span>
</div>
</div>
</div>
<div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
<div class="block-18">
<div class="icon"><span class="flaticon-doctor"></span></div>
<div class="text">
<strong class="number" data-number="1151">0</strong>
<span>Successful Students</span>
</div>
</div>
</div>
<div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
<div class="block-18">
<div class="icon"><span class="flaticon-doctor"></span></div>
 <div class="text">
<strong class="number" data-number="964">0</strong>
<span>Happy Parents</span>
</div>
</div>
</div>
<div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
<div class="block-18">
<div class="icon"><span class="flaticon-doctor"></span></div>
<div class="text">
<strong class="number" data-number="312">0</strong>
<span>Awards Won</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="ftco-section testimony-section bg-light">
<div class="container">
<div class="row justify-content-center mb-5 pb-2">
<div class="col-md-8 text-center heading-section ftco-animate">
<h2 class="mb-4"><span>Our Students </span> Saying About Us</h2>
<p>Some glimpse of the students saying about the school.</p>
</div>
</div>
<div class="row ftco-animate justify-content-center">
<div class="col-md-12">
<div class="carousel-testimony owl-carousel">
	@foreach($testinomial as $test)
<div class="item">
<div class="testimony-wrap d-flex">
<div class="user-img mr-4" style="background-image: url({{asset('storage/images/profileImages/' . $test->image)}})">
</div>
<div class="text ml-2 bg-light">
<span class="quote d-flex align-items-center justify-content-center">
<i class="icon-quote-left"></i>
</span>
<p>{{$test->body}}</p>
<p class="name">{{$test->name}}</p>
<span class="position">{{$test->desg}}</span>
</div>
</div>
</div>
@endforeach

</div>
</div>
</div>
</div>
</section>
<section class="ftco-section ftco-consult ftco-no-pt ftco-no-pb" style="background-image: url(/images/bg_5.jpg);">
<div class="container">
<div class="row justify-content-end">
<div class="col-md-6 py-5 px-md-5 bg-primary">
<div class="heading-section heading-section-white ftco-animate mb-5">
<h2 class="mb-4">Inquiry</h2>
<p>For more info about the school and students we help you to find the details accordingly.</p>
</div>
@if (count($errors) > 0)
    <div class="alert alert-danger">
     <button type="button" class="close" data-dismiss="alert">×</button>
     <ul>
      @foreach ($errors->all() as $error)
       <li>{{ $error }}</li>
      @endforeach
     </ul>
    </div>
   @endif
   @if ($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
<form action="/sendinquiry" method="POST" class="appointment-form ftco-animate">
	{!! csrf_field() !!}
<div class="d-md-flex">
<div class="form-group">
<input type="text" name="first_name" class="form-control" placeholder="Full Name">
</div>
<div class="form-group ml-md-4">
<input type="text" name="last_name"class="form-control" placeholder="email">
</div>
</div>
<div class="d-md-flex">
<!-- <div class="form-group">
<div class="form-field">
<div class="select-wrap">
<div class="icon"><span class="lnr lnr-chevron-down"></span></div>
<select name="" id="" class="form-control">
<option value="">Select Your Course</option>
<option value="">Art Lesson</option>
<option value="">Language Lesson</option>
<option value="">Music Lesson</option>
<option value="">Sports</option>
<option value="">Other Services</option>
</select>
</div>
</div>
</div> -->
<div class="form-group ml-md-8">
<input type="text" name="phone" class="form-control" placeholder="Phone">
</div>
</div>
<div class="d-md-flex">
<div class="form-group">
<textarea name="message" id="" cols="30" rows="6" class="form-control" placeholder="Message"></textarea>
</div>
</div>
<div class="form-group ml-md-4">
<input type="submit" value="Submit" class="btn btn-secondary py-3 px-4">
</div>
</form>
</div>
</div>
</div>
</section>
<section class="ftco-section bg-light">
<div class="container">
<div class="row justify-content-center mb-5 pb-2">
<div class="col-md-8 text-center heading-section ftco-animate">
<h2 class="mb-4"><span>Recent</span> Blog</h2>
</div>
</div>
<div class="row">
	<?php
                if(count($post)>0)
                {
                foreach($post as $key=>$pos){
                ?>
                @if($key <= 5)
<div class="col-md-6 col-lg-4 ftco-animate">
<div class="blog-entry">
<a href="{{url("post/$pos->slug/show")}}" class="block-20 d-flex align-items-end" style="background-image: url('<?php echo $pos->image; ?>');">
<div class="meta-date text-center p-2">

</div>
</a>
<div class="text bg-white p-4">
<h3 class="heading"><a href="{{url("post/$pos->slug/show")}}"><?php echo str_limit($pos->title, $limit = 37, $end = '..');  ?></a></h3>
<p><?php  echo str_limit($pos->body, $limit = 150, $end = '...');  ?></p>
<div class="d-flex align-items-center mt-4">
<p class="mb-0"><a href="{{url("post/$pos->slug/show")}}" class="btn btn-secondary">Read More <span class="lnr lnr-arrow-right"></span></a></p>
<p class="ml-auto mb-0">
 <a href="#" class="mr-2">Admin</a>
<a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a>
</p>
</div>
</div>
</div>
</div>
@endif
<?php

}
}
?>

</div>
</div>
</section>
<section class="ftco-gallery heading-section text-center">
	<h2 style="margin: 25px auto;"><span>Our</span> Gallery</h2>
<div class="container-wrap">
<div class="row no-gutters">
<div class="col-md-3 ftco-animate">
<a href="{{asset('/images/gallary/Class1.jpg')}}" class="gallery image-popup img d-flex align-items-center" style="background-image: url({{asset('images/gallary/Class1.jpg')}});">
<div class="icon mb-4 d-flex align-items-center justify-content-center">
<span class="icon-instagram"></span>
</div>
</a>
</div>
<div class="col-md-3 ftco-animate">
<a href="{{asset('/images/gallary/IMG_2256.jpg')}}" class="gallery image-popup img d-flex align-items-center" style="background-image: url({{asset('images/gallary/IMG_2256.jpg')}});">
<div class="icon mb-4 d-flex align-items-center justify-content-center">
<span class="icon-instagram"></span>
</div>
</a>
</div>
<div class="col-md-3 ftco-animate">
<a href="{{asset('/images/gallary/IMG_1664.jpg')}}" class="gallery image-popup img d-flex align-items-center" style="background-image: url({{asset('images/gallary/IMG_1664.jpg')}});">
<div class="icon mb-4 d-flex align-items-center justify-content-center">
<span class="icon-instagram"></span>
</div>
</a>
</div>
<div class="col-md-3 ftco-animate">
<a href="{{asset('/images/gallary/IMG_1081.jpg')}}" class="gallery image-popup img d-flex align-items-center" style="background-image: url({{asset('images/gallary/IMG_1081.jpg')}});">
<div class="icon mb-4 d-flex align-items-center justify-content-center">
<span class="icon-instagram"></span>
</div>
</a>
</div>
<div class="col-md-3 ftco-animate">
<a href="{{asset('/images/gallary/IMG_2729.jpg')}}" class="gallery image-popup img d-flex align-items-center" style="background-image: url({{asset('images/gallary/IMG_2729.jpg')}});">
<div class="icon mb-4 d-flex align-items-center justify-content-center">
<span class="icon-instagram"></span>
</div>
</a>
</div>
<div class="col-md-3 ftco-animate">
<a href="{{asset('/images/gallary/IMG_1232.jpg')}}" class="gallery image-popup img d-flex align-items-center" style="background-image: url({{asset('images/gallary/IMG_1232.jpg')}});">
<div class="icon mb-4 d-flex align-items-center justify-content-center">
<span class="icon-instagram"></span>
</div>
</a>
</div>
<div class="col-md-3 ftco-animate">
<a href="{{asset('/images/gallary/Class7.jpg')}}" class="gallery image-popup img d-flex align-items-center" style="background-image: url({{asset('images/gallary/Class7.jpg')}});">
<div class="icon mb-4 d-flex align-items-center justify-content-center">
<span class="icon-instagram"></span>
</div>
</a>
</div>
<div class="col-md-3 ftco-animate">
<a href="{{asset('/images/gallary/IMG_1945.jpg')}}" class="gallery image-popup img d-flex align-items-center" style="background-image: url({{asset('images/gallary/IMG_1945.jpg')}});">
<div class="icon mb-4 d-flex align-items-center justify-content-center">
<span class="icon-instagram"></span>
</div>
</a>
</div>
</div>
</div>
</section>



@endsection