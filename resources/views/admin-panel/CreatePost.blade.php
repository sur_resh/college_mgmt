
<?php

?>
<div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div>
<div class="create-post-form">

    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>CREATE NEW POST</h4></div>

                <div class="panel-body">
                    {!! Form::open(array('url'=>'/create-post/','method'=>'POST','files' => true )) !!}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="col-sm-12 ">
                            <div class="panel panel-default ">

                                <div class="panel-body">
                                    <div class="formn-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                        <label for="title" class="col-md-12 control-label">Title</label>

                                        <div class="col-md-12">
                                            <input id="txt_email" type="text" class="form-control input-lg" name="title" value="{{ old('title') }}" required autofocus>

                                            @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="slug" class="col-md-12 control-label">Slug</label>

                                        <div class="inputr-group  col-md-12">
                                            <input id="txt_username" type="text" class="form-control" name="slug" >


                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="catag_id" class="col-md-12 control-label">Category</label>
                                        <div class="col-md-12 col-sm-12">

                                            {{ Form:: select('category_id',$categories,null,array("class" => "form-control margin-bottom-12", 'placeholder'=>'Select Any catag', 'id' => 'catag_id')) }}


                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                                        <label for="body"   class="col-md-12 control-label">body</label>
                                        <div class="col-md-12">
                                         <textarea name="body"  id="ckeditor_text" class="tinymce form-control my-editor"></textarea>

                                          @if ($errors->has('body'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('body') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('meta_description') ? ' has-error' : '' }}">
                                        <label for="meta_description" class="col-md-12 control-label">Meta Description</label>

                                        <div class="col-md-12">
                                            <input id="meta_description" type="text" class="form-control" name="meta_description" value="{{ old('meta_description') }}" required autofocus>

                                            @if ($errors->has('meta_description'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('meta_description') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('meta_keywords') ? ' has-error' : '' }}">
                                        <label for="meta_keywords" class="col-md-12 control-label">Meta Keywords</label>

                                        <div class="col-md-12">
                                            <input id="meta_keywords" type="text" class="form-control" name="meta_keywords" value="{{ old('meta_keywords') }}" required autofocus>

                                            @if ($errors->has('meta_keywords'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('meta_keywords') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="status" class="col-md-12 control-label">Status</label>
                                        <div class="col-md-12 col-sm-12">
                                            {{ Form:: select('status', $status, array("class" => "form-control margin-bottom-12", 'placeholder'=>'Select Any Status', 'id' => 'status')) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-6 margin-left-20">
                                        {!! Form::label('featured_image','Select Featured Image',array('id'=>'','class'=>'margin-top-20')) !!}
                                        {!! Form::file('image',array('class'=>'imageUpload','id'=>'uploadFile')) !!}
                                        <div class="form-group userprofile-bg" id="imagePreview">
                                        </div>    
                                    </div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    Create
                                                </button>
                                            </div>
                                        </div>   
                                         </div>
                                    </div>
                                </div>
                            </div>
                            {{ Form:: close() }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
   

        <script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
        <script>


        $('#txt_email').keyup(function () {



            var str = $(this).val();
            str= str.toLowerCase();
            str = str.replace(/\s+/g, "-");

            $("#txt_username").val(str);

        });


        </script>
        @include('mceImageUpload::upload_form')

