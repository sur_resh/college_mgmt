<div class="home-section display-page" xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <div class="col-sm-6  col-md-4 text-left">
            <div class="manage-users">Page List</div>

        </div>

        <div class="col-sm-6 text-right text-xs-center">
            <span class="create-user"><a href="/userProfile">check it</a> <a href="/"><img
                            src="/images/plus.png"></a></span>
        </div>
    </div>
    <div class="table-header">
        <div class="row">


            <div class="col-sm-4 col-md-4 text-left margin-bottom-8">
                <h4>Page List</h4>
            </div>
            <div class="col-sm-4 col-md-4 margin-bottom-8">
                {!! Form::open(array('url'=>'/search-user','method'=>'POST','files' => true )) !!}
                <input type="text" class="searchuser" name="username" placeholder="Search">
                <button type="submit" class="btn btn-danger btnusersearch">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </button>
                {{ Form:: close() }}
            </div>


            <div class="col-sm-3 padding-left-0 margin-bottom-8">
                <select placeholder="Filter By" class="searchuser userddl">
                    <option>Filter By</option>
                    <option>Recently Updated</option>
                    <option>Recently Deleted</option>
                </select>
            </div>
            <div class="clear-both" style="clear:both;"></div>
            <div class="col-md-4 col-sm-4  margin-bottom-0">
                <p class="Page-info-head">Title</p>
            </div>
            <div class="col-md-3 col-sm-1  margin-bottom-0">
                <p class="Page-info-head">Slug</p>
            </div>
            <div class="col-md-1 col-sm-1  margin-bottom-0">
                <p class="Page-info-head">Author</p>
            </div>
          {{--  <div class="col-md-2 col-sm-1 margin-bottom-0">
                <p class="Page-info-head">Meta Description</p>
            </div>
            <div class="col-sm-1 margin-bottom-0">
                <p class="Page-info-head">Meta Keywords</p>
            </div>--}}
            <div class="col-sm-1 margin-bottom-0">
                <p class="Page-info-head">Status</p>
            </div>
            <div class="col-sm-3 margin-bottom-0">
                <p class="Page-info-head">Edit/Delete</p>
            </div>
        </div>
    </div>
    <div class="content-body">
        @foreach($page_infos as $pageinfo)
            <div class="user-row">
                <div class="row">

                    <div class="col-md-4 col-sm-4 margin-bottom-0">
                        <span class="user-red"><b>{{ $pageinfo->title}}</b></span>

                    </div>

                    <div class="col-md-3 col-sm-1 margin-bottom-0">
                        <span class="user-red"><b>{{ $pageinfo->slug}}</b></span>

                    </div>

                    <div class="col-md-1 col-sm-1 margin-bottom-0">
                        <span class="user-red"><b><?php
                                if($pageinfo->author_id==$author_name['Student']){
                               echo "Student";
                                }
                                else if($pageinfo->author_id==$author_name['Administrator']){
                                    echo "Administrator";
                                }
                                else if($pageinfo->author_id==$author_name['Teacher']){
                                    echo "Teacher";
                                }
                                else if($pageinfo->author_id==$author_name['Parents']){
                                    echo "Parents";
                                }


                                    ?></b></span>

                    </div>
                {{--   <div class="col-md-2 col-sm-3 margin-bottom-0">
                        <span class="user-red"><b>{{ $pageinfo->meta_description}}</b></span>

                    </div>
                    <div class="col-md-1 col-sm-3 margin-bottom-0">
                        <span class="user-red"><b>{{ $pageinfo->meta_keywords}}</b></span>

                    </div>--}}
                    <div class="col-md-1 col-sm-3 margin-bottom-0">
                        <span class="user-red"><b>{{ $pageinfo->status}}</b></span>

                    </div>



                    <div class="col-sm-3 margin-bottom-0">
                        <a href="/pages/{{$pageinfo->id}}/edit" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>  Edit</a>
                        <a href="/pages/{{$pageinfo->id}}/delete" onclick="return confirm('Are you sure?')" class="user-delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>

                    </div>
                </div>

            </div>

        @endforeach()
    </div>
</div>