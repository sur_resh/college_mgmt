@extends('layouts.mainpage.app')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-4 sidebar12">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-8 sidebar13">
            @include('admin-panel.create_category')
        </div>
        <div class="col-sm-12 sidebar14">

            @include('admin-panel.DisplayCategory')

        </div>
    </div>
</div>
    <script type="text/javascript" src="js/imagePreview.js"></script>
@endsection