<div class="side-bar">
    <div class="post-sidebar">
    <h4>Recent Posts</h4>

    @foreach($sidebar as $bar)
        <li><a href="/post/{{$bar->slug}}/show">{{str_limit($bar->title, $limit = 30, $end = '...') }}</a></li>
        @endforeach
    </div>
    <div class="category-sidebar">
       <h4>Categories</h4>
        @foreach($catag as $cat)
            <li><a href="/Catagory/{{$cat->id}}">{{str_limit($cat->name, $limit = 30, $end = '...') }}</a></li>
        @endforeach
    </div>
{{--    <div class="news-sidebar">
        <h4>Latest News</h4>
        @foreach($news as $new)
            <li><a href="/post/{{$new->slug}}/show">{{str_limit($new->title, $limit = 30, $end = '...') }}</a></li>
        @endforeach
    </div>
    <div class="upcommingevent-sidebar">
        <h4>Upcomming Events</h4>
        @foreach($upevents as $events)
            <li><a href="/post/{{$events->slug}}/show">{{str_limit($events->title, $limit = 30, $end = '...') }}</a></li>
        @endforeach
    </div>--}}

    </div>