@extends('layouts.mainpage.app')

@section('content')


@endsection
@section('nav')
<section class="dashboard-page">
<div class="container">
    <div class="row">
        <div class="col-sm-3 col-md-3">
    @include('admin-panel/left-nav')
</div>
            <div class="col-sm-9 col-md-9">
    @include('admin-panel/DashboardHeader')
                @include('admin-panel/DashboardBody')
</div>
    </div>
    </div>
</section>
@endsection
