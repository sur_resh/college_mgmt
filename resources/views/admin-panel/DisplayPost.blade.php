<div class="home-section display-post" xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <div class="col-sm-6  col-md-4 text-left">
            <div class="manage-users"><h2 class="panel-heading ">Post List</h2></div>

        </div>


    </div>
    <div class="table-header">
        <div class="row">


            

            <div class="col-md-4 col-sm-4  margin-bottom-0">
                <h4 class="Page-info-head"><strong>Title</strong></h4>
            </div>
            <div class="col-md-1 col-sm-1  margin-bottom-0">
                <h4 class="Page-info-head"><strong>Category</strong></h4>
            </div>
            <div class="col-md-2 col-sm-1 margin-bottom-0">
                <h4 class="Page-info-head"><strong>Author</strong></h4>
            </div>
            <div class="col-sm-2 margin-bottom-0">
                <h4 class="Page-info-head"><strong>Meta Keywords</strong></h4>
            </div>
            <div class="col-sm-1 margin-bottom-0">
                <h4 class="Page-info-head"><strong>Status</strong></h4>
            </div>
            <div class="col-sm-2 margin-bottom-0">
                <h4 class="Page-info-head"><strong>Edit/Delete</strong></h4>
            </div>
        </div>
    </div>
    <div class="content-body">
        @foreach($post_infos as $pageinfo)
            <div class="user-row">
                <div class="row">

                    <div class="col-md-4 col-sm-4 margin-bottom-0">
                        <span class="user-red"><b>{{ $pageinfo->title}}</b></span>

                    </div>

                    <div class="col-md-1 col-sm-1 margin-bottom-0">
                        <span class="user-red"><b>{{ $pageinfo->name}}</b></span>

                    </div>
                    <div class="col-md-2 col-sm-1 margin-bottom-0">
                        <span class="user-red"><b><?php
                                if($pageinfo->author_id==$author_name['Student']){
                                    echo "Student";
                                }
                                else if($pageinfo->author_id==$author_name['Administrator']){
                                    echo "Administrator";
                                }
                                else if($pageinfo->author_id==$author_name['Teacher']){
                                    echo "Teacher";
                                }
                                else if($pageinfo->author_id==$author_name['Parents']){
                                    echo "Parents";
                                }


                                ?></b></span>

                    </div>
                    <div class="col-md-2 col-sm-3 margin-bottom-0">
                        <span class="user-red"><b>{{ $pageinfo->meta_description}}</b></span>

                    </div>

                    <div class="col-md-1 col-sm-3 margin-bottom-0">
                        <span class="user-red"><b>{{ $pageinfo->status}}</b></span>

                    </div>



                    <div class="col-sm-2 margin-bottom-0">
                        <a href="/post/{{$pageinfo->id}}/edit" class="user-edit">Edit</a>
                        <a href="/post/{{$pageinfo->id}}/delete" onclick="return confirm('Are you sure?')" class="user-delete"> Delete</a>

                    </div>
                </div>

            </div>

        @endforeach()
    </div>
</div>


