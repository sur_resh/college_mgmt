@extends('layouts.mainpage.app')

@section('content')
    <div class="row">
        <div class="col-sm-4">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-8">
            @include('admin-panel.page_edit')
        </div>
    </div>
    <script type="text/javascript" src="js/imagePreview.js"></script>
@endsection