
<div class="panel-group" id="accordion">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-th">
                            </span>General</a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse">
            <div class="panel-body">
                <table class="table">

                    <tr>
                        <td>
                            <a href="/add-testinomial">Add Testinomial</a>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <a href="/testinomial">Display Testinomial</a>
                        </td>
                    </tr>
                     <!-- <tr>
                        <td>
                            <a href="/send-message">Send Message to Parents</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="/contact-display">Display Messages</a>
                        </td>
                    </tr>  -->

                </table>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-file">
                        </span>Posts</a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td>
                            <span class="glyphicon glyphicon-usd"></span><a href="/posts">Post</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="glyphicon glyphicon-user"></span><a href="/create-post">Create Post</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="glyphicon glyphicon-tasks"></span><a href="/create-categories">Category</a>
                        </td>
                    </tr>
                    {{-- <tr>
                         <td>
                             <span class="glyphicon glyphicon-shopping-cart"></span><a href="http://www.jquery2dotnet.com"></a>
                         </td>
                     </tr>--}}
                </table>
            </div>
        </div>
    </div>


    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapsethree"><span class="glyphicon glyphicon-file">
                        </span>Modals</a>
            </h4>
        </div>
        <div id="collapsethree" class="panel-collapse collapse">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td>
                            <span class="glyphicon glyphicon-usd"></span><a href="/modal">Modal</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="glyphicon glyphicon-user"></span><a href="/modal/create">Create Modal</a>
                        </td>
                    </tr>
                    <tr>
                        <!-- <td>
                            <span class="glyphicon glyphicon-tasks"></span><a href="/create-categories">Category</a>
                        </td> -->
                    </tr>
                    {{-- <tr>
                         <td>
                             <span class="glyphicon glyphicon-shopping-cart"></span><a href="http://www.jquery2dotnet.com"></a>
                         </td>
                     </tr>--}}
                </table>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><span class="glyphicon glyphicon-user">
                        </span>Slider</a>
        </h4>
    </div>
    <div id="collapseSix" class="panel-collapse collapse">
        <div class="panel-body">
            <table class="table">
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-usd"></span><a href="/slider">Slider</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-user"></span><a href="/slider/create">Create Slider</a>
                    </td>
                </tr>

                {{-- <tr>
                     <td>
                         <span class="glyphicon glyphicon-shopping-cart"></span><a href="http://www.jquery2dotnet.com"></a>
                     </td>
                 </tr>--}}
            </table>
        </div>
    </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">

            <li> <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                    <i class="fa fa-circle-o text-red"></i>
                    Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form></li>
        </div>
    </div>
</div>





