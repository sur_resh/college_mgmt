@extends('layouts.mainpage.app')

@section('content')
    <div class="container">
    <div class="row">
        <div class="col-sm-4">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-8">
            @include('admin-panel.DisplayPost')
        </div>
    </div>
    <script type="text/javascript" src="js/imagePreview.js"></script>
    </div>
@endsection