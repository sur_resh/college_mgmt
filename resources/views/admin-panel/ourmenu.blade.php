@extends('layouts.mainpage.app')

@section('contents')
    {!! Menu::render() !!}
@endsection
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
//YOU MUST HAVE JQUERY LOADED BEFORE menu scripts
@push('scripts')

{!! Menu::scripts() !!}
@endpush