



<div class="container">
<div class="page-edit-section">
    <div class="table-header"><h4 class="panel-heading ">UPADATE CATEGORIES</h4></div>

    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
    {{ csrf_field() }}

    <div class="row">
        {!! Form::model($CategoryInfos,['method'=>'PATCH','files' => true,'action'=>['CategoriesController@update',$CategoryInfos->id]]) !!}

        <div class="col-sm-12 ">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form-group{{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Name</label>
                        <div class="col-md-12 col-sm-12">

                            {{ Form:: text('name', $CategoryInfos->name,  array("class" => "form-control margin-bottom-12", 'id' => 'name')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="slug" class="col-md-4 control-label">Slug</label>
                        <div class="col-md-12 col-sm-12">
                            {{ Form::text('slug',$CategoryInfos->slug, array("class" => "form-control margin-bottom-12", 'id' => 'slug')) }}


                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">

                        <label for="body" class="col-md-4 control-label">Body</label>
                        <div class="col-md-12 col-sm-12">

                            {{Form::textarea('body',$CategoryInfos->body, array("class"=>"form-control margin-bottom-12",'id'=>'body'))}}

                        </div>
                    </div>







                    <div class="form-group">
                        <div class="col-sm-12 edit_botton">

                            {{ Form::submit('Update', array('class' => 'btn btn-primary', 'id' => 'submit_id')) }}

                        </div>
                    </div>

                </div>
            </div>
        </div>
        {{ Form:: close() }}
    </div>
    <div class="row">
        <div class="col-sm-12">
            @include('layouts.error_message')
            @include('layouts.sucess_message')
        </div>
    </div>

</div>
</div>