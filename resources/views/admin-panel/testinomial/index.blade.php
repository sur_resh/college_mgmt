@extends('layouts.mainpage.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-4 sidebar12">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-8 sidebar13">
            <div class="row">
                <div class="col-sm-12">
                    @include('layouts.error_message')
                    @include('layouts.sucess_message')
                </div>
            </div>
            <div class="create-post-form">

                <div class="row">
                    <div class="col-md-12 ">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h4>TESTINOMIAL</h4></div>

                            <div class="panel-body">
                                {!! Form::open(array('url'=>'/testinomial/','method'=>'POST','files' => true )) !!}
                                {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                                {{ csrf_field() }}
                                <div class="row">

                                    <div class="col-sm-12 ">
                                        <div class="panel panel-default ">

                                            <div class="panel-body">


                                                <table class="table table-hover">
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Name</th>
                                                        <th>Designation</th>
                                                        <th>Image</th>
                                                        <th>Body</th>
                                                        <th>Action</th>
                                                    </tr>

                                                    @foreach ($testinomial as $test)
                                                        <tr>
                                                            <td>{{$test->id}}</td>
                                                            <td>{{$test->name}}</td>
                                                            <td>{{$test->desg}}</td>
                                                            <td><img style="max-width:50%;" src="{{ asset('storage/images/profileImages/' . $test->image)}}"></td>
                                                            <td>{{$test->body}}</td>
                                                            <td>
                                                                <a href="/testinomial/{{$test->id}}/edit" class="user-edit">Edit</a>
                                                                <a href="/testinomial/{{$test->id}}/delete" onclick="return confirm('Are you sure?')" class="user-delete">Delete</a>

                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>









                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{ Form:: close() }}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection





