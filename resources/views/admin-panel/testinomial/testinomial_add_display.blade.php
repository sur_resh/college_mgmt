
<?php

?>
<div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div>
<div class="create-post-form">

    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>CREATE NEW TESTINOMIAL</h4></div>

                <div class="panel-body">
                    {!! Form::open(array('url'=>'/add-testinomial/','method'=>'POST','files' => true )) !!}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="col-sm-12 ">
                            <div class="panel panel-default ">

                                <div class="panel-body">
                                    <div class="formn-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-12 control-label">Name</label>

                                        <div class="col-md-12">
                                            <input id="name" type="text" class="form-control input-lg" name="name" value="{{ old('name') }}" required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('desg') ? ' has-error' : '' }}">
                                        <label for="desg" class="col-md-12 control-label">Designation</label>

                                        <div class="col-md-12">
                                            <input id="desg" type="text" class="form-control" name="desg" value="{{ old('desg') }}" required autofocus>

                                            @if ($errors->has('desg'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('desg') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                                        <label for="body" class="col-md-12 control-label">Message</label>

                                        <div class="col-md-12">
                                            <input id="body" type="text" class="form-control" name="body" value="{{ old('body') }}" required autofocus>

                                            @if ($errors->has('body'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-6 margin-left-20">
                                        {!! Form::label('featured_image','Select Featured Image',array('id'=>'image','class'=>'margin-top-20')) !!}
                                        {!! Form::file('image',array('class'=>'imageUpload','id'=>'uploadFile')) !!}
                                        <div class="form-group userprofile-bg" id="imagePreview">

                                        </div>    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Create
                                            </button>
                                        </div>
                                    </div>   
                                     </div>
                            </div>
                        </div>
                    </div>
                    {{ Form:: close() }}
                </div>
            </div>
        </div>
    </div>

</div>



