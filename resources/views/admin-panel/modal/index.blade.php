
@extends('layouts.mainpage.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                @include('admin-panel.left-nav')
            </div>
            <div class="col-sm-8">
                <div class="content-header">
                    <div class="container">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark">Modal</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <section class='content'>
                    <div class='container'>

                        <div class='row'>
                            @foreach($modal as $m)
                                <div class="col-sm-4">
                                    <div class="wrap">
                                        <h4>Name:  {{$m->name}}</h4>
                                        
                                        <img src="{{ asset('/images/'.$m->photo )}}" style="width:300px;">
                                        <a href="/modal/delete/{{$m->id}}" class="btn btn-danger">Delete</a>

                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </section>
            </div>
        </div>
       <br>
    </div>




@endsection
