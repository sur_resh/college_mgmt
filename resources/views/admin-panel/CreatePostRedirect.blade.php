@extends('layouts.mainpage.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-4 sidebar12">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-8 sidebar13">
            @include('admin-panel.CreatePost')
        </div>
    </div>
</div>

@endsection