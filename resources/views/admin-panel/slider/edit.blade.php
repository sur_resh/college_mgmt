@extends('layouts.mainpage.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                @include('admin-panel.left-nav')
            </div>
            <div class="col-sm-8">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark">Edit Slider</h1>
                            </div>
                            <div class="col-sm-6">

                            </div>
                        </div>
                    </div>
                </div>
                <section class='content'>
                    <div class='container-fluid'>
                        <form method="POST" action="/slider/edit/{{$slider->id}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" value="{{$slider->name}}" class="form-control" required/>
                            </div>
                            <div class="form-group">
                                <label for="short_intro">Short Intro</label>
                                <input type="text" name="short_intro" value="{{$slider->short_intro}}" class="form-control" required/>
                            </div>
                            <div class="form-group">
                                <label for="intro">Slider</label>
                                <input type="text" name="intro" value="{{$slider->intro}}" class="form-control" required/>
                            </div>
                            <div class="form-group">
                                <label for="photo">Image</label>
                                <input type="file" name="photo" accept="photo/jpg, photo/png, photo/jpeg" ><br>
                                <img src="{{ asset('/images/'.$slider->photo )}}" style="width:250px;">
                            </div>
                            <div>
                                <button type='submit' class='button is-link'>Save</button><br><br>
                            </div>
                        </form>

                    </div>
                </section>
            </div>
        </div>
        <script type="text/javascript" src="js/imagePreview.js"></script>
    </div>


@endsection