@extends('layouts.mainpage.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-4">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-8">
            <section class='content'>
                <div class='container-fluid'>
                    <div class="content-header">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1 class="m-0 text-dark">Slider</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form method="POST" action="/slider/create" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" name="name" placeholder="name" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <input type="text" name="short_intro" placeholder="short intro" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <input type="text" name="intro" placeholder="intro" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <input type="file" name="photo" accept="image/jpg, image/png, image/jpeg">
                        </div>
                        <div>

                            <button type='submit' class='button is-link'>save</button>
                        </div>
                            <br>

                    </form>

                </div>
            </section>
        </div>
    </div>
</div>

@endsection