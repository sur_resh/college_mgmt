
@extends('layouts.mainpage.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                @include('admin-panel.left-nav')
            </div>
            <div class="col-sm-8">
                <div class="content-header">
                    <div class="container">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark">Slider</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <section class='content'>
                    <div class='container'>

                        <div class='row'>
                            @foreach($slider as $s)
                                <div class="col-sm-4">
                                    <div class="wrap">
                                        <h4>Name:  {{$s->name}}</h4>
                                        <h5>Short Intro:{{$s->short_intro}}</h5>
                                        <h5>Description:{{$s->intro}}</h5>
                                        <img src="{{ asset('/images/'.$s->photo )}}" style="width:300px;">
                                        <a href="/slider/edit/{{$s->id}}" class="btn btn-info">Edit</a>
                                        <a href="/slider/delete/{{$s->id}}" class="btn btn-danger">Delete</a>

                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </section>
            </div>
        </div>
       <br>
    </div>




@endsection
