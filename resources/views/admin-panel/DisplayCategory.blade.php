<div class="home-section display-category" xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <div class="col-sm-6  col-md-4 text-left">
            <div class="manage-users"><h2>Categories List</h2></div>

        </div>


   </div>
  
     


<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Name</th>
      <th scope="col">Slug</th>
      <th scope="col">Description</th>
      <th scope="col">Edit/Delete</th>
    </tr>
  </thead>
  <tbody>
      @foreach($category_infos as $pageinfo)
    <tr>
      <th scope="row">{{ $pageinfo->id}}</th>
      <td>{{ $pageinfo->name}}</td>
      <td>{{ $pageinfo->slug}}</td>
      <td>{{ $pageinfo->body}}</td>
      <td> <a href="/category/{{$pageinfo->id}}/edit" class="user-edit">Edit</a>
           <a href="/category/{{$pageinfo->id}}/delete" onclick="return confirm('Are you sure?')" class="user-delete"> Delete</a></td>
    </tr>
      @endforeach()
   
  </tbody>
</table>
</div>