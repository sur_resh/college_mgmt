

<?php

?>
<div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div>
<div class="create-category-form">

    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>CREATE CATEGORIES</h4></div>

                <div class="panel-body">
                    {!! Form::open(array('url'=>'/create-categories/','method'=>'POST','files' => true )) !!}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="panel panel-default ">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="name" class="col-md-4 control-label">Name</label>

                                        <div class="col-md-12">
                                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="slug" class="col-md-4 control-label">Slug</label>

                                        <div class="col-md-12">
                                            <input id="slug" type="text" class="form-control" name="slug" value="{{ old('slug') }}" required autofocus>

                                            @if ($errors->has('slug'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="body" class="col-md-4 control-label">Description</label>

                                        <div class="col-md-12">
                                            <input id="body" type="textarea" class="form-control" name="body" value="{{ old('body') }}" required autofocus>

                                            @if ($errors->has('body'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Create
                                            </button>
                                        </div>
                                    </div>    </div>
                            </div>
                        </div>
                    </div>
                    {{ Form:: close() }}
                </div>
            </div>
        </div>
    </div>

</div>