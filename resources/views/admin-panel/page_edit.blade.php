
<div class="page-edit-section">
    <div class="row">
        <div class="col-sm-12">
            @include('layouts.error_message')
            @include('layouts.sucess_message')
        </div>
    </div>
    <div class="table-header"><h4 class="panel-heading ">UPADATE PAGE</h4></div>

    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
    {{ csrf_field() }}

    <div class="row">
        {!! Form::model($pageInfos,['method'=>'PATCH','files' => true,'action'=>['PageController@update',$pageInfos->id]]) !!}

        <div class="col-sm-8 ">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form-group{{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="title" class="col-md-4 control-label">Title</label>
                        <div class="col-md-10 col-sm-12">

                            {{ Form:: text('title',null,  array("class" => "form-control margin-bottom-12", 'id' => 'title')) }}
                        </div>
                    </div>
                    <div class="panel-body">
                        <label for="title" class="col-md-4 control-label">Parmalink</label>
                        <a href="{{url("pages/$pageInfos->slug/show")}}">{{url("pages/$pageInfos->slug/show")}}</a>

                    </div>
                {{--    <div class="form-group">
                        <label for="author-id" class="col-md-4 control-label">ROLE</label>
                        <div class="col-md-10 col-sm-12">
                            {{ Form:: select('author_id', $role, null, array("class" => "form-control margin-bottom-12", 'placeholder'=>'Select Any Role', 'id' => 'role')) }}


                        </div>
                    </div>--}}
                    <div class="form-group">
                        <label for="slug" class="col-md-4 control-label">Slug</label>
                        <div class="col-md-10 col-sm-12">
                            {{ Form::text('slug', null, array("class" => "form-control margin-bottom-12", 'id' => 'slug')) }}


                        </div>
                    </div>
                    <div class="form-group">
                        <label for="body" class="col-md-4 control-label">body</label>

                        <div class="col-md-10 col-sm-12">

                            <script src="http://cdn.tinymce.com/4/tinymce.min.js"></script>
                            {{form::textarea ('body',$pageInfos->body, array("class" => "tinymce form-control my-editor margin-bottom-12", 'id' => 'body'))}}

                            <script>
                                var editor_config = {
                                    path_absolute : "/",
                                    selector: "textarea.my-editor",
                                    plugins: [
                                        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                                        "searchreplace wordcount visualblocks visualchars code fullscreen",
                                        "insertdatetime media nonbreaking save table contextmenu directionality",
                                        "emoticons template paste textcolor colorpicker textpattern"
                                    ],
                                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                                    relative_urls: false,
                                    file_browser_callback : function(field_name, url, type, win) {
                                        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                                        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                                        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                                        if (type == 'image') {
                                            cmsURL = cmsURL + "&type=Images";
                                        } else {
                                            cmsURL = cmsURL + "&type=Files";
                                        }

                                        tinyMCE.activeEditor.windowManager.open({
                                            file : cmsURL,
                                            title : 'Filemanager',
                                            width : x * 0.8,
                                            height : y * 0.8,
                                            resizable : "yes",
                                            close_previous : "no"
                                        });
                                    }
                                };

                                tinymce.init(editor_config);
                            </script>



                        </div>
                    </div>






                    <div class="form-group">
                        <label for="meta_description" class="col-md-4 control-label">Meta Description</label>

                        <div class="col-md-10 col-sm-12">
                            {{ Form::text('meta_description', null, array("class" => "form-control margin-bottom-12", 'id' => 'meta_description')) }}


                        </div>
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords" class="col-md-4 control-label">Meta Keywords</label>
                        <div class="col-md-10 col-sm-12">
                            {{ Form::text('meta_keywords', null, array("class" => "form-control margin-bottom-12", 'id' => 'meta_keywords')) }}


                        </div>
                    </div>

                    <div class="form-group">
                        <label for="status" class="col-md-4 control-label">Status</label>
                        <div class="col-md-10 col-sm-12">
                            {{ Form:: select('status', $status, null, array("class" => "form-control margin-bottom-12", 'placeholder'=>'Select Any Role', 'id' => 'status')) }}


                        </div>
                    </div>
                    <div class="col-sm-3 margin-left-20">

                        {{ csrf_field() }}
                        {!! Form::label('featured_image','Select Featured Image',array('id'=>'','class'=>'margin-top-20')) !!}
                        {!! Form::file('image',null,array('class'=>'imageUpload','id'=>'uploadFile')) !!}

                        <div class="form-group userprofile-bg" id="imagePreview">
                            <img src="/{{ $pageInfos->image }}"  width="100%" id="editimage"/>
                        </div></div>
                    <div class="form-group">
                        <div class="col-sm-12">

                            {{ Form::submit('Update', array('class' => 'btn btn-primary', 'id' => 'submit_id')) }}

                        </div>
                    </div>

                </div>
            </div>
        </div>
        {{ Form:: close() }}
    </div>


</div>