<div class="home-section" xmlns="http://www.w3.org/1999/html">
    <div class="row">

        <div class="col-sm-12  col-md-4 text-left">
            <div class="manage-users"> User Profile</div>

        </div>

    </div>

    <div class="content-body profile-content">
        @include('layouts.sucess_message')
        @include('layouts.error_message')
        <div class="row">
            <div class="col-sm-6">
                @foreach($user_infos as $userinfo)
                    <div class="row">
                        <div class="col-sm-3">
                            <img src="/{{$userinfo->image_path}}" class="rounded-profile">
                        </div>
                        <div class="col-sm-9">

                            <div class="profile-name">{{$userinfo->name}}</div>
                            <div class="profile-role">@if($userinfo->role_id==1) Administrator @else User @endif</div>
                            <div class="profile-email">{{$userinfo->email}}</div>

                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-sm-3 col-sm-offset-3 text-right">
                <div class="row">
                    <div class="col-sm-12">
                        <a href="/userinfo/{{$userinfo->id}}/edit" class="user-edit user-edit-profile"><i class="fa fa-pencil"
                                                                                                             aria-hidden="true"></i> Edit</a>
                    </div>
                    <div class="col-sm-12">
                        <a href="/userinfo/{{$userinfo->id}}/delete" onclick="return confirm('Are you sure?')"
                           class="user-delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                    </div>
                </div>

            </div>

        </div>
        {!! Form::open(array('url'=>'/update-password','method'=>'POST','files' => true )) !!}
        <div class="row">
            <div class="col-sm-12">
                <div class="update-password"> Update Password</div>
            </div>
            <div class="col-sm-12">
                <input id="password" type="password" class="form-control margin-bottom-12 profile-password" placeholder="Password" name="password" required>

            </div>
            <div class="col-sm-12">
                <input id="password_confirmation" type="password" class="form-control margin-bottom-12 profile-confirm-password" placeholder="Confirm Password"
                       name="password_confirmation" required>
                @foreach($user_infos as $userinfo)
                    <input id="id" type="hidden" name="id" value="{{$userinfo->id}}" required>
                @endforeach
            </div>
            <div class="col-sm-12">
                {{ Form::submit('Update', array('class' => 'btn btn-primary user-edit user-edit-profile text-uppercase', 'id' => 'submit_id')) }}
            </div>

        </div>

        {{ Form:: close() }}



    </div>

</div>
