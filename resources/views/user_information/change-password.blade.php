@extends('layouts.app')

@section('content')

    <div class="col-sm-3">
    @include('admin-panel.left-nav')
    {{--@include('vendor.voyager.deviceinfo.deviceinfo')--}}
    </div>
    <div class="col-sm-9">
    @include('user_information.change-password-form')
</div>
    <div class="clear-both" style="clear:both;"></div>
@endsection