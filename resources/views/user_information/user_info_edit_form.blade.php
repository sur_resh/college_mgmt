<div class="user-register-section">
    <div class="table-header"><h4 class="panel-heading ">UPDATE USER</h4></div>

    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
    {{ csrf_field() }}

    <div class="row">
        {!! Form::model($userInfos,['method'=>'PATCH','files' => true,'action'=>['ProfileController@update',$userInfos->id]]) !!}
        <div class="col-sm-3 margin-left-20">

            {{ csrf_field() }}
            {!! Form::label('featured_image','Select Featured Image',array('id'=>'','class'=>'margin-top-20')) !!}
            {!! Form::file('image',array('class'=>'imageUpload','id'=>'uploadFile')) !!}

            <div class="form-group userprofile-bg" id="imagePreview">
                <img src="/{{ $userInfos->image_path }}"  width="100%" id="editimage"/>
            </div></div>
        <div class="col-sm-8 ">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form-group{{ $errors->has('name') ? 'has-error' : '' }}">

                        <div class="col-md-10 col-sm-12">

                            {{ Form:: text('name',null,  array("class" => "form-control margin-bottom-12", 'id' => 'name')) }}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">


                        <div class="col-md-10 col-sm-12">

                            {{Form::text('email',null, array("class"=>"form-control margin-bottom-12",'id'=>'email'))}}

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10 col-sm-12">
                            {{ Form:: select('role_id', $role, null, array("class" => "form-control margin-bottom-12", 'placeholder'=>'Select Any Role', 'id' => 'role')) }}


                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10 col-sm-12">
                            {{ Form::text('phone_number', null, array("class" => "form-control margin-bottom-12",'placeholder'=>'Your Contact No.', 'id' => 'phone_number')) }}


                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10 col-sm-12">
                            {{ Form::text('address', null, array("class" => "form-control margin-bottom-12",'placeholder'=>'Your Address', 'id' => 'address')) }}


                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">


                        <div class="col-md-10 col-sm-12">


                            <input id="password" type="password" class="form-control margin-bottom-12" placeholder="Password" name="password" >

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10 col-sm-12">
                            <input id="password-confirm" type="password" class="form-control margin-bottom-12" placeholder="Confirm Password"
                                   name="password_confirmation" >
                        </div>
                    </div>




                    <div class="form-group">
                        <div class="col-sm-6">

                            {{ Form::submit('Submit', array('class' => 'btn btn-primary', 'id' => 'submit_id')) }}

                        </div>
                    </div>

                </div>
            </div>
        </div>
        {{ Form:: close() }}
    </div>
    <div class="row">
        <div class="col-sm-12">
            @include('layouts.error_message')
            @include('layouts.sucess_message')
        </div>
    </div>

</div>