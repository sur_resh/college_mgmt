<div class="home-section vehicle-list" xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <div class="col-sm-6  col-md-4 text-left">
            <div class="manage-users"> Manage Users.</div>

        </div>

        <div class="col-sm-6 text-right text-xs-center">
            <span class="create-user"><a href="/userProfile">Create User</a> <a href="/userProfile"><img
                            src="/images/plus.png"></a></span>
        </div>
    </div>
    <div class="table-header">
        <div class="row">


            <div class="col-sm-4 col-md-4 text-left margin-bottom-8">
                <h4>User List</h4>
            </div>
            <div class="col-sm-4 col-md-4 margin-bottom-8">
                {!! Form::open(array('url'=>'/search-user','method'=>'POST','files' => true )) !!}
                <input type="text" class="searchuser" name="username" placeholder="Search">
                <button type="submit" class="btn btn-danger btnusersearch">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </button>
                {{ Form:: close() }}
            </div>


            <div class="col-sm-3 padding-left-0 margin-bottom-8">
                <select placeholder="Filter By" class="searchuser userddl">
                    <option>Filter By</option>
                    <option>Recently Updated</option>
                    <option>Recently Deleted</option>
                </select>
            </div>
            <div class="clear-both" style="clear:both;"></div>
            <div class="col-md-1 col-sm-2  margin-bottom-0">
            <p class="user-info-head">Image</p>
                </div>
            <div class="col-md-2 col-sm-2  margin-bottom-0">
                <p class="user-info-head">Role</p>
                </div>
            <div class="col-md-3 col-sm-3 margin-bottom-0">
                <p class="user-info-head">Name</p>
                </div>
            <div class="col-sm-3 margin-bottom-0">
                <p class="user-info-head">E-mail</p>
                </div>
            <div class="col-sm-3 margin-bottom-0">
                <p class="user-info-head">Edit/Delete</p>
                </div>
        </div>
    </div>
    <div class="content-body">
        @foreach($user_infos as $userinfo)
            <div class="user-row">
                <div class="row">

                    <div class="col-md-1 col-sm-2  margin-bottom-0">
                        <img src=" /{{ $userinfo->image_path}}" class="user-pic"/>
                    </div>
                    <div class="col-md-2 col-sm-2  margin-bottom-0">
                       <span class="user-role"><b> <?php $h= $userinfo->role_id ;
                               switch($h)

                               {
                                   case 1: echo 'Administrator';
                                       break;
                                   case 2: echo 'Student';
                                       break;
                                   case 3: echo 'Teacher';
                                       break;
                                   case 4: echo 'Parents';
                                       break;
                                   default: echo 'child';

                               }
                               ?></b></span>
                    </div>
                    <div class="col-md-3 col-sm-3 margin-bottom-0">
                        <span class="user-red"><b>{{ $userinfo->name}}</b></span>

                    </div>
                    <div class="col-sm-3 margin-bottom-0">
                        {{ $userinfo->email}}
                    </div>

                    <div class="col-sm-3 margin-bottom-0">
                        <a href="/userinfo/{{$userinfo->id}}/edit" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>  Edit</a>
                        <a href="/userinfo/{{$userinfo->id}}/delete" onclick="return confirm('Are you sure?')" class="user-delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>

                    </div>
                </div>

            </div>

        @endforeach()
    </div>
</div>