<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Styles -->

    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>{{--
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
            <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>
                <a href="{{ route('register') }}">Register</a>
                @endauth
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
        --}}{{--    {{$menuList}}--}}{{--
        </div>


    </div>
</div>--}}
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Spangle School</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            @foreach($menuList as $pageinfo=>$value)

                <ul class="nav navbar-nav">

                    @if(count($value['child'])>0)
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ $value['label']}} <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                @foreach($value['child'] as $childkey=>$childValue)
                                    <li><a href="#">{{$childValue['label']}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    @else
                        <li class="active"><a href="#">{{ $value['label']}}</a>

                        </li>
                    @endif

                </ul>
            @endforeach

        </div>
</nav>
{{--
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Spangle School</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            @foreach($menuList as $pageinfo=>$value)
            <ul class="nav navbar-nav">
@if(count($value['child'])>0)

                    @if(count($value['child'])>0)
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ $value['label']}} <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                @foreach($value['child'] as $childkey=>$childValue)
                                    <li><a href="#">{{$childValue['label']}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    @else
                        <li class="active"><a href="#">{{ $value['label']}}</a>

                        </li>
                    @endif

            </ul>
            @endforeach

        </div>
</nav>--}}
</body>
</html>
