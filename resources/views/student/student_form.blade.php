
<?php

?>
<div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div>
<div class="student-form">

    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h4> STUDENT FORM</h4></div>

                <div class="panel-body">
                    {!! Form::open(array('url'=>'/student-form','method'=>'POST','files' => true )) !!}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="col-sm-12 ">
                            <div class="panel panel-default ">

                                <div class="panel-body">

                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 control-label">Name</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="class_id" class="col-md-4 control-label">Class</label>
                                        <div class="col-md-6 col-sm-6">

                                            {{ Form:: select('class_id',$class_id,null,array("class" => "form-control margin-bottom-12", 'placeholder'=>'Select Any Class', 'id' => 'class_id')) }}


                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 control-label">E-Mails</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                        <label for="phone_number" class="col-md-4 control-label">Phone Number</label>

                                        <div class="col-md-6">
                                            <input id="phone_number" type="tel" class="form-control" name="phone_number" value="{{ old('phone_number') }}" required>

                                            @if ($errors->has('phone_number'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="blood" class="col-md-4 control-label">Blood Group</label>
                                        <div class="col-md-6 col-sm-6">

                                            {{ Form:: select('Blood',$blood,null,array("class" => "form-control margin-bottom-12", 'placeholder'=>'Select Any Group', 'name' => 'Blood')) }}


                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('p_name') ? ' has-error' : '' }}">
                                        <label for="p_name" class="col-md-4 control-label">Parent's Name</label>

                                        <div class="col-md-6">
                                            <input id="p_name" type="text" class="form-control" name="p_name" value="{{ old('p_name') }}" required autofocus>

                                            @if ($errors->has('p_namename'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('p_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('pemail') ? ' has-error' : '' }}">
                                        <label for="pemail" class="col-md-4 control-label">Parent's E-Mails</label>

                                        <div class="col-md-6">
                                            <input id="pemail" type="pemail" class="form-control" name="pemail" value="{{ old('pemail') }}" required>

                                            @if ($errors->has('pemail'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('pemail') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('p_phone_number') ? ' has-error' : '' }}">
                                        <label for="p_phone_number" class="col-md-4 control-label">Parent's Phone Number</label>

                                        <div class="col-md-6">
                                            <input id="p_phone_number" type="tel" class="form-control" name="p_phone_number" value="{{ old('p_phone_number') }}" required>

                                            @if ($errors->has('p_phone_number'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('p_phone_number') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                        <label for="address" class="col-md-4 control-label">Address</label>

                                        <div class="col-md-6">
                                            <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required>

                                            @if ($errors->has('address'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>





                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Submit
                                            </button>
                                        </div>
                                    </div>    </div>
                            </div>
                        </div>
                    </div>
                    {{ Form:: close() }}
                </div>
            </div>
        </div>
    </div>

</div>