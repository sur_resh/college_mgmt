
<div class="student-edit-section">
    <div class="row">
        <div class="col-sm-12">
            @include('layouts.error_message')
            @include('layouts.sucess_message')
        </div>
    </div>
    <div class="table-header"><h4 class="panel-heading ">EDIT STUDENT INFORMATION</h4></div>

    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
    {{ csrf_field() }}

    <div class="row">
        {!! Form::model($stu_Infos,['method'=>'PATCH','files' => true,'action'=>['StudentController@update',$stu_Infos->id]]) !!}

        <div class="col-sm-12 ">
            <div class="panel panel-default">

                <div class="panel-body">


                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Name</label>

                        <div class="col-md-6">
                            {{ Form:: text('name',null,  array("class" => "form-control margin-bottom-12", 'id' => 'name')) }}

                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="class_id" class="col-md-4 control-label">Class</label>
                        <div class="col-md-6 col-sm-6">

                            {{ Form:: select('class_id',$class_id,null,array("class" => "form-control margin-bottom-12", 'placeholder'=>'Select Any Class', 'id' => 'class_id')) }}


                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mails</label>

                        <div class="col-md-6">
                            {{ Form:: text('email',null,  array("class" => "form-control margin-bottom-12", 'id' => 'email')) }}

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                        <label for="phone_number" class="col-md-4 control-label">Phone Number</label>

                        <div class="col-md-6">
                            {{ Form:: text('phone_number',null,  array("class" => "form-control margin-bottom-12", 'id' => 'phone_number')) }}

                            @if ($errors->has('phone_number'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="blood" class="col-md-4 control-label">Blood Group</label>
                        <div class="col-md-6 col-sm-6">

                            {{ Form:: select('Blood',$blood,null,array("class" => "form-control margin-bottom-12", 'placeholder'=>'Select Any Class', 'name' => 'Blood')) }}


                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('p_name') ? ' has-error' : '' }}">
                        <label for="p_name" class="col-md-4 control-label">Parent's Name</label>

                        <div class="col-md-6">
                            {{ Form:: text('p_name',null,  array("class" => "form-control margin-bottom-12", 'id' => 'p_name')) }}

                            @if ($errors->has('p_name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('p_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('pemail') ? ' has-error' : '' }}">
                        <label for="pemail" class="col-md-4 control-label">Parent's E-Mails</label>

                        <div class="col-md-6">
                            {{ Form:: text('pemail',null,  array("class" => "form-control margin-bottom-12", 'id' => 'pemail')) }}

                            @if ($errors->has('pemail'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('pemail') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('p_phone_number') ? ' has-error' : '' }}">
                        <label for="p_phone_number" class="col-md-4 control-label">Parent's Phone Number</label>

                        <div class="col-md-6">
                            {{ Form:: text('p_phone_number',null,  array("class" => "form-control margin-bottom-12", 'id' => 'p_phone_number')) }}

                            @if ($errors->has('p_phone_number'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('p_phone_number') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
<div class="clear"></div>

                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                        <label for="address" class="col-md-4 control-label">Address</label>

                        <div class="col-md-6">
                            {{ Form:: text('address',null,  array("class" => "form-control margin-bottom-12", 'id' => 'address')) }}

                            @if ($errors->has('address'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-12">

                            {{ Form::submit('Update', array('class' => 'btn btn-primary', 'id' => 'submit_id')) }}

                        </div>
                    </div>



            </div>
        </div>
        {{ Form:: close() }}
    </div>


</div>