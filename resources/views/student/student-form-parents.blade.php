
<?php

?>
<div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div>
<div class="student-form-parents">

    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h4> MESSAGE TO PARENTS</h4></div>

                <div class="panel-body">
                    {!! Form::open(array('url'=>'/info-message-store','method'=>'POST','files' => true )) !!}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="col-sm-12 ">
                            <div class="panel panel-default ">

                                <div class="panel-body">

                                    <div class="form-group{{ $errors->has('tittle') ? ' has-error' : '' }}">
                                        <label for="tittle" class="col-md-4 control-label">Tittle</label>

                                        <div class="col-md-6">
                                            <input id="tittle" type="text" class="form-control" name="tittle" value="{{ old('tittle') }}" required autofocus>

                                            @if ($errors->has('tittle'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('tittle') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="class_id" class="col-md-4 control-label">Class</label>
                                        <div class="col-md-6 col-sm-6">

                                            {{ Form:: select('class_id',$class_id,null,array("class" => "form-control margin-bottom-12", 'placeholder'=>'Select Any Class', 'name' => 'class_id')) }}


                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 control-label">Sender Email</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('m_name') ? ' has-error' : '' }}">
                                        <label for="m_name" class="col-md-4 control-label">Message Sender</label>

                                        <div class="col-md-6">
                                            <input id="m_name" type="text" class="form-control" name="m_name" value="{{ old('m_name') }}" required>

                                            @if ($errors->has('m_name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('m_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                        <label for="phone_number" class="col-md-4 control-label"> Contact Number</label>

                                        <div class="col-md-6">
                                            <input id="phone_number" type="tel" class="form-control" name="phone_number" value="{{ old('phone_number') }}" required>

                                            @if ($errors->has('phone_number'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                                        <label for="message" class="col-md-4 control-label">Message</label>

                                        <div class="col-md-6">
                                            <input id="message" type="text" class="form-control" name="message" value="{{ old('message') }}" required autofocus>

                                            @if ($errors->has('message'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>











                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Submit
                                            </button>
                                        </div>
                                    </div>    </div>
                            </div>
                        </div>
                    </div>
                    {{ Form:: close() }}
                </div>
            </div>
        </div>
    </div>

</div>