
<?php

?>
<div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div>
<div class="student-form-parents">

    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h4> MESSAGE TO PARENTS</h4></div>

                <div class="panel-body">
                    {!! Form::open(array('url'=>'/send-message-store','method'=>'POST','files' => true )) !!}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="col-sm-12 ">
                            <div class="panel panel-default ">

                                <div class="panel-body">

                                    <div class="form-group{{ $errors->has('tittle') ? ' has-error' : '' }}">
                                        <label for="tittle" class="col-md-4 control-label">Title</label>

                                        <div class="col-md-6">
                                            <input id="tittle" type="text" class="form-control" name="tittle" value="{{ old('tittle') }}" required autofocus>

                                            @if ($errors->has('tittle'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('tittle') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>





                                    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                                        <label for="message" class="col-md-4 control-label">Message</label>

                                        <div class="col-md-6">
                                            <input id="message" type="text" class="form-control" name="message" value="{{ old('message') }}" required autofocus>

                                            @if ($errors->has('message'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>











                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Submit
                                            </button>
                                        </div>
                                    </div>    </div>
                            </div>
                        </div>
                    </div>
                    {{ Form:: close() }}
                </div>
            </div>
        </div>
    </div>

</div>