@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-3">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-9">

            @include('marksheet.show_student');
        </div>
    </div>

@endsection