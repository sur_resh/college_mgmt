{{--$pdf = App::make('dompdf.wrapper');
$pdf->loadHTML('--}}
<div id="contenjt" style="">
    <div id="features" class="marks-card-heading col-sm-12 ">
        <h4>SPANGLE BORDING SCHOOL</h4>
        <h6>Putalisadak,Kathmandu</h6>

    </div>
    <div class="col-sm-3 logo-spangle">
        <img src="/images/1/school-logo.png"></div>
    <div class="col-sm-9">

        <div class="row marks-card-student-details">

            <div class="col-sm-4">
                @foreach($stu_info as $info)
                    <p>Name:<span>{{$info->name}}</span></p>
                    <p>Address:<span>{{$info->address}}</span></p>
                    <p>Phone Number:<span>{{$info->phone_number}}</span></p>
                @endforeach
            </div>
            <div class="col-sm-4">
                @foreach($stu_info as $info)
                    <p>Email:<span>{{$info->email}}</span></p>
                    <p>Class:<span>{{$info->class}}</span></p>
                    <p>Roll no:<span>{{$info->class}}SP{{$info->id}}</span></p>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row ">
        <div class="col-sm-12">
            <div class="tab-content">
                <div id="class1" class="tab-pane fade in active">


                    <table id="example" class="display" cellspacing="0" width="100%">
                        <thead>
                        <tr class="heading">

                            <th>Subjects</th>
                            <th>Full Marks</th>
                            <th>Pass Marks</th>
                            <th>Obtained Marks</th>
                        </tr>

                        </thead>

                        <tbody>


                        @foreach($student as $stu_info)

                            <tr class="student-info">

                                {{-- <td>
                                     <a href="" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>  Edit</a>
                                     <a href="" onclick="return confirm('Are you sure?')" class="user-delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                                     --}}{{--@foreach($class as $key=>$value)

                                         {{$}};
                                         --}}{{----}}{{--      @if($value==$stu_info->class_id)
                                            {{ $stu_info->class_id}}
                         @endif--}}{{----}}{{--
                                         @endforeach--}}{{--

                                 </td>--}}


                                <td class="odd">{{$stu_info->subject}}</td>
                                <td class="even">{{$stu_info->fmarks}}</td>
                                <td class="odd">{{$stu_info->pamarks}}</td>
                                <td class="even">{{$stu_info->marks}}</td>
                            </tr>

                        @endforeach
                        </tbody>

                    </table>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="total-marks">
                                <div class="col-sm-6"></div>
                                <div class="col-md-2 total">
                                    <p>Total</p>

                                </div>
                                <div class="col-sm-3">
                                    @foreach($markinfo as $mark_info)
                                        <p>{{$mark_info->total}}</p>
                                </div>
                                @endforeach
                            </div>
                            <div class="percentage">
                                <div class="col-sm-6"></div>
                                <div class="col-md-2 total">
                                    <p>Percentage</p>

                                </div>
                                <div class="col-sm-3">
                                    @foreach($markinfo as $mark_info)
                                        <p>{{$mark_info->percentage}}.%</p>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--');
return $pdf->stream();--}}
<div id="editor"></div>
<button id="cmd">generate PDF</button>



<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
<script>

    var doc = new jsPDF();

    var specialElementHandlers = {
        '#editor': function (element, renderer) {
            return true;
        }
    };

    $('#cmd').click(function () {

        doc.fromHTML($('#content').html(), 15, 15, {
            'width': 170,
            'elementHandlers': specialElementHandlers
        });

        doc.save('sample-file.pdf');
    });
</script>