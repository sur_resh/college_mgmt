<section class="check-result">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <h4><span>Attention!!</span> Select your class and check your result</h4>
                <div class="col-sm-1 classes">
                    <a href="/check-result/1" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>Montessori</a>
                </div>
                <div class="col-sm-1 classes">
                    <a href="/check-result/2" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>Nursery</a>
                </div>
                <div class="col-sm-1 classes">
                    <a href="/check-result/3" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>LKG</a>
                </div>
                <div class="col-sm-1 classes">
                    <a href="/check-result/4" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>UKG</a>
                </div>
                <div class="col-sm-1 classes">
                    <a href="/check-result/5" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>Class 1</a>
                </div>
                <div class="col-sm-1 classes">
                    <a href="/check-result/6" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>Class 2</a>
                </div>
                <div class="col-sm-1 classes">
                    <a href="/check-result/7" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>Class 3</a>
                    </div>
                <div class="col-sm-1 classes">
                    <a href="/check-result/8" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>Class 4</a>
                </div>
                <div class="col-sm-1 classes">
                    <a href="/check-result/9" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>Class 5</a>
                </div>
                <div class="col-sm-1 classes">
                    <a href="/check-result/10" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>Class 6 </a>
                </div>
                <div class="col-sm-1 classes">
                    <a href="/check-result/11" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>Class 7</a>
                </div>
                <div class="col-sm-1 classes">
                    <a href="/check-result/12" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>Class 8</a>
                </div>
                <div class="col-sm-1 classes">
                    <a href="/check-result/13" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>Class 9</a>
                </div>
                <div class="col-sm-1 classes">
                    <a href="/check-result/14" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>Class 10</a>
                </div>
            </div>

        </div>
    </div>

</section>

