



<div class="tab-content">
    <div id="class1" class="tab-pane fade in active">
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

        <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
        <script>
            $(document).ready(function() {
                $('#example').DataTable( {
                    "scrollY":        "200px",
                    "scrollCollapse": true,
                    "paging":         true,
                } );
            } );

        </script>

        <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>

                <th>Name</th>
                <th>Total Marks</th>
                <th>Percentage</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Name</th>
                <th>Total Marks</th>
                <th>Percentage</th>

            </tr>
            </tfoot>
            <tbody>


            @foreach($student as $stu_info)

                <tr>

                    {{-- <td>
                         <a href="" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>  Edit</a>
                         <a href="" onclick="return confirm('Are you sure?')" class="user-delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                         --}}{{--@foreach($class as $key=>$value)

                             {{$}};
                             --}}{{----}}{{--      @if($value==$stu_info->class_id)
                                {{ $stu_info->class_id}}
             @endif--}}{{----}}{{--
                             @endforeach--}}{{--

                     </td>--}}
                    <td><a href="/show/result/{{$stu_info->id}}">{{$stu_info->name}}</a></td>

                    <td>{{$stu_info->total}}</td>
                    <td>{{$stu_info->percentage}}</td>

                </tr>

            @endforeach
            </tbody>
        </table>
    </div>
</div>