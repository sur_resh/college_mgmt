@extends('layouts.mainpage.app')

@section('content')

<div class="jumbotron jumbotron-sm contact-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h1 class="">
                   <small>Gallery</small>
                </h1>
            </div>
        </div>
    </div>
</div>
<div class="container gallery-container">

    <h1>Gallery</h1>

    <p class="page-description text-center">School Gallery</p>
    
    <div class="tz-gallery">

        <div class="row">
            @foreach ($gallery as $galle)
            <div class="col-sm-6 col-md-3">
                <?php 
                    $string = $galle->image;
                    $string = substr(strrchr($string, '.'), 1);
                ?>
                @if($string == "jpg" || $string == "png" || $string == "jpeg")
                <a class="lightbox" href="../uploads/gallery/{{$galle->image}}">
                    <img src="../uploads/gallery/{{$galle->image}}" alt="{{$galle->name}}">
                </a>
                <div class="caption form-control">
                        <h3 style="text-align:center">{{$galle->name}}</h3>
                </div>
                @else
                <video class="pb-video-frame my-lightbox" width="100%" height="230" src="../uploads/gallery/{{$galle->image}}" frameborder="0" allowfullscreen controls></video>
                <label class="form-control label-warning text-center">{{$galle->name}}</label>
                @endif
            </div>
            @endforeach
     </div>
</div>
</div>

@endsection